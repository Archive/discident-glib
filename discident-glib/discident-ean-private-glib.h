/*
   Copyright (C) 2010 Bastien Nocera

   The Gnome Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   The Gnome Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with the Gnome Library; see the file COPYING.LIB.  If not,
   write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301  USA.

   Authors: Bastien Nocera <hadess@hadess.net>

 */

#ifndef DISCIDENT_EAN_PRIVATE_GLIB_H
#define DISCIDENT_EAN_PRIVATE_GLIB_H

#include <libsoup/soup.h>

struct DiscidentEanPrivate {
	char *service;

	/* RL */
	char *server;
	gboolean enabled;
	char *message;
	gboolean login_done;

	/* Amazon */
	char *access_key;
	char *private_key;
	char *associate_tag;
};


typedef struct {
	DiscidentEan       *ean;
	GSimpleAsyncResult *simple;
	SoupSession        *session;
	char               *barcode;
} QueryData;

void free_query_data (QueryData *data);
char *_sign_query (const char  *url,
		   const char  *timestamp,
		   const char  *private_key,
		   const char **steps);

gboolean _rl_parse_lookup_response (const char  *response,
				    char       **ret_title,
				    char       **ret_img_url);
gboolean _amz_parse_lookup_response (const char  *response,
				     char       **ret_title,
				     char       **ret_img_url,
				     GError     **error);

#endif /* DISCIDENT_EAN_PRIVATE_GLIB_H */
