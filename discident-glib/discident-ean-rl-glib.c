/*
   Copyright (C) 2010 Bastien Nocera

   The Gnome Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   The Gnome Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with the Gnome Library; see the file COPYING.LIB.  If not,
   write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301  USA.

   Authors: Bastien Nocera <hadess@hadess.net>

 */

#include <string.h>

#include <libsoup/soup.h>
#include <glib/gprintf.h>
#include <libxml/tree.h>
#include <libxml/parser.h>

#include "discident-ean-glib.h"
#include "discident-ean-rl-glib.h"
#include "discident-error.h"

#define APPID "19d78263508549b98db3809824f79c4d0fcad11b"
#define EMULATED_VERSION "2.7.0"
#define ORIGINAL_QUERY "http://redlaser.com:8008/getinfo.ashx?v=" EMULATED_VERSION "&app=rl&responseId=20100708&udid=" APPID
#define SEARCH_URL "http://%s/searchresults.ashx"
#define SEARCH_QUERY "barcode=%s&btype=EAN13&cachedimages=ebay_small,googlelogo_24,thefindlogo&currency=GBP&locale=%s&udid=" APPID "&v=" EMULATED_VERSION

static gboolean
parse_login_response (DiscidentEan *ean,
		      char         *response)
{
	GHashTable *hash;
	char **items;
	guint i;
	const char *disable_capture;

	/* Shame we won't be using the response,
	 * but we've already logged in */
	if (ean->priv->server != NULL)
		return TRUE;

	hash = g_hash_table_new_full (g_str_hash, g_str_equal,
				      g_free, g_free);

	/* Remove trailing '|' */
	if (response[strlen (response) - 1] == '|')
		response[strlen (response) - 1] = '\0';

	items = g_strsplit (response, ",", -1);
	for (i = 0; items[i] != NULL; i++) {
		char **vars;

		vars = g_strsplit (items[i], "=", -1);
		if (items[0] == NULL || items[1] == NULL) {
			g_strfreev (items);
			continue;
		}
		g_hash_table_insert (hash, g_ascii_strdown (vars[0], -1), g_strdup (vars[1]));
		g_strfreev (vars);
	}
	g_strfreev (items);

	ean->priv->server = g_strdup (g_hash_table_lookup (hash, "ssvr"));
	if (ean->priv->server == NULL) {
		g_hash_table_destroy (hash);
		return FALSE;
	}
	ean->priv->message = g_strdup (g_hash_table_lookup (hash, "disablecapturemessage"));
	disable_capture = g_hash_table_lookup (hash, "disablecapture");
	if (disable_capture != NULL &&
	    g_ascii_strncasecmp (disable_capture, "no", 2) != 0) {
		ean->priv->enabled = FALSE;
	}

	ean->priv->login_done = TRUE;

	g_hash_table_destroy (hash);

	return TRUE;
}

static gboolean
discident_ean_login_sync (DiscidentEan *ean,
			  GError      **error)
{
	SoupSession *session;
	SoupMessage *msg;
	char *response;
	int ret;

	g_return_val_if_fail (DISCIDENT_IS_EAN (ean), FALSE);

	session = soup_session_new ();

	msg = soup_message_new ("GET", ORIGINAL_QUERY);
	g_assert (msg != NULL);

	response = NULL;

	ret = soup_session_send_message (session, msg);
	if (SOUP_STATUS_IS_SUCCESSFUL (ret) &&
	    msg->response_body != NULL) {
		response = g_strdup (msg->response_body->data);
	}
	g_object_unref (msg);
	g_object_unref (session);

	if (response == NULL) {
		g_set_error (error, SOUP_HTTP_ERROR, ret, "Could not login to EAN service");
		return FALSE;
	}

	if (parse_login_response (ean, response) == FALSE) {
		g_set_error (error, DISCIDENT_ERROR, DISCIDENT_ERROR_PARSE, "Failed to parse login response from EAN service");
		g_free (response);
		return FALSE;
	}
	g_free (response);

	return TRUE;
}

static char *
get_search_uri (DiscidentEan *ean)
{
	return g_strdup_printf (SEARCH_URL, ean->priv->server);
}

static char *
uncompress (const char *data,
	    gssize      len)
{
	GConverter *converter;
	GInputStream *input_stream, *stream;
	GOutputStream *output;
	char *ret;

	input_stream = g_memory_input_stream_new ();
	converter = G_CONVERTER (g_zlib_decompressor_new (G_ZLIB_COMPRESSOR_FORMAT_GZIP));
	stream = (GInputStream *) g_converter_input_stream_new (input_stream, converter);
	output = (GOutputStream *) g_memory_output_stream_new (NULL, 0, g_realloc, g_free);

	g_memory_input_stream_add_data ((GMemoryInputStream *) input_stream,
					g_memdup (data, len), len,
					(GDestroyNotify) g_free);

	if (!g_output_stream_splice (G_OUTPUT_STREAM (output), stream,
				     G_OUTPUT_STREAM_SPLICE_CLOSE_SOURCE,
				     NULL, NULL)) {
		g_object_unref (output);
		return NULL;
	}

	ret = g_strdup (g_memory_output_stream_get_data ((GMemoryOutputStream *) output));
	g_object_unref (output);

	/* Probably not compressed */
	if (ret == NULL)
		return g_strndup (data, len);

	return ret;
}

gboolean
_rl_parse_lookup_response (const char  *response,
			   char       **ret_title,
			   char       **ret_img_url)
{
	xmlDocPtr doc;
	xmlChar *prop;

	if (response == NULL)
		return FALSE;

	doc = xmlParseMemory (response, strlen (response));
	if (doc == NULL)
		doc = xmlRecoverMemory (response, strlen (response));

	if(!doc ||
	   !doc->children ||
	   !doc->children->name ||
	   g_ascii_strcasecmp ((char *)doc->children->name, "response") != 0) {
		if (doc != NULL)
			xmlFreeDoc (doc);
		return FALSE;
	}

	prop = xmlGetProp (doc->children, (const xmlChar *) "title");
	if (prop == NULL) {
		xmlFreeDoc (doc);
		return FALSE;
	}
	*ret_title = g_strdup ((const char *) prop);
	xmlFree (prop);

	prop = xmlGetProp (doc->children, (const xmlChar *) "imageUrl");
	if (prop != NULL)
		*ret_img_url = g_strdup ((const char *) prop);
	xmlFree (prop);

	xmlFreeDoc (doc);

	return TRUE;
}

static SoupMessage *
create_query_message (DiscidentEan *ean,
		      const char   *barcode)
{
	SoupMessage *msg;
	char *uri, *data;

	uri = get_search_uri (ean);
	msg = soup_message_new ("POST", uri);
	g_free (uri);

	data = g_strdup_printf (SEARCH_QUERY, barcode, ean->priv->service + strlen ("rl-"));
	soup_message_set_request (msg, "application/x-www-form-urlencoded",
				  SOUP_MEMORY_TAKE, data, strlen (data));

	return msg;
}

gboolean
discident_ean_rl_lookup_sync (DiscidentEan *ean,
			      const char   *barcode,
			      char        **title,
			      char        **img_url,
			      GError      **error)
{
	SoupSession *session;
	SoupMessage *msg;
	char *response;
	int ret;

	g_return_val_if_fail (DISCIDENT_IS_EAN (ean), FALSE);
	g_return_val_if_fail (title != NULL, FALSE);

	if (ean->priv->login_done == FALSE) {
		if (discident_ean_login_sync (ean, error) == FALSE)
			return FALSE;
	}

	session = soup_session_new ();

	msg = create_query_message (ean, barcode);

	response = NULL;

	ret = soup_session_send_message (session, msg);
	if (SOUP_STATUS_IS_SUCCESSFUL (ret) &&
	    msg->response_body != NULL) {
		response = uncompress (msg->response_body->data,
				       msg->response_body->length);
	}
	g_object_unref (msg);
	g_object_unref (session);

	if (response == NULL) {
		g_set_error (error, SOUP_HTTP_ERROR, ret, "Could not query EAN service");
		return FALSE;
	}

	if (_rl_parse_lookup_response (response, title, img_url) == FALSE) {
		g_free (response);
		g_set_error (error, DISCIDENT_ERROR, DISCIDENT_ERROR_PARSE, "Failed to parse response from EAN service");
		return FALSE;
	}

	if (g_strcmp0 (*title, barcode) == 0) {
		g_free (response);
		g_set_error (error, DISCIDENT_ERROR, DISCIDENT_ERROR_EMPTY_RESPONSE, "No response for requested barcode");
		return FALSE;
	}

	g_free (response);

	return TRUE;
}

static void
got_body_query (SoupMessage *msg,
		QueryData   *data)
{
	const char *barcode;
	char *response;
	char *title = NULL;
	char *img_url = NULL;

	if (!SOUP_STATUS_IS_SUCCESSFUL (msg->status_code) ||
	    msg->response_body == NULL) {
		g_simple_async_result_set_error (data->simple,
						 SOUP_HTTP_ERROR,
						 msg->status_code,
						 "Could not query EAN service: %s",
						 soup_status_get_phrase (msg->status_code));
		goto out;
	}

	response = uncompress (msg->response_body->data,
			       msg->response_body->length);

	if (_rl_parse_lookup_response (response, &title, &img_url) == FALSE) {
		g_simple_async_result_set_error (data->simple,
						 DISCIDENT_ERROR,
						 DISCIDENT_ERROR_PARSE,
						 "Failed to parse response from EAN service");
		g_free (response);
		goto out;
	}
	g_free (response);

	barcode = g_object_get_data (G_OBJECT (data->simple), "barcode");
	if (g_strcmp0 (title, barcode) == 0) {
		g_simple_async_result_set_error (data->simple,
						 DISCIDENT_ERROR,
						 DISCIDENT_ERROR_EMPTY_RESPONSE,
						 "No response for requested barcode");
		g_free (title);
		g_free (img_url);
		goto out;
	}

	g_object_set_data_full (G_OBJECT (data->simple), "image-url", img_url, g_free);
	g_simple_async_result_set_op_res_gpointer (data->simple, title, NULL);

out:
	g_simple_async_result_complete_in_idle (data->simple);
	free_query_data (data);
}

static void
got_body_login (SoupMessage *msg,
		QueryData   *data)
{
	char *response;
	SoupMessage *query_msg;

	if (!SOUP_STATUS_IS_SUCCESSFUL (msg->status_code) ||
	    msg->response_body == NULL) {
		g_simple_async_result_set_error (data->simple,
						 SOUP_HTTP_ERROR,
						 msg->status_code,
						 "Could not login to EAN service: %s",
						 soup_status_get_phrase (msg->status_code));
		goto error;
	}

	response = g_strndup (msg->response_body->data,
			      msg->response_body->length);

	if (parse_login_response (data->ean, response) == FALSE) {
		g_simple_async_result_set_error (data->simple,
						 DISCIDENT_ERROR,
						 DISCIDENT_ERROR_PARSE,
						 "Failed to parse login response from EAN service");
		g_free (response);
		goto error;
	}

	g_free (response);

	query_msg = create_query_message (data->ean, data->barcode);
	g_signal_connect (G_OBJECT (query_msg), "got-body",
			  G_CALLBACK (got_body_query), data);
	soup_session_queue_message (data->session, query_msg, NULL, NULL);
	return;

error:
	g_simple_async_result_complete_in_idle (data->simple);
	free_query_data (data);
}

void
discident_ean_rl_lookup (DiscidentEan        *ean,
			 SoupSession         *session,
			 QueryData           *data)
{
	SoupMessage *msg;

	if (ean->priv->login_done == FALSE) {
		msg = soup_message_new ("GET", ORIGINAL_QUERY);
		g_signal_connect (G_OBJECT (msg), "got-body",
				  G_CALLBACK (got_body_login), data);
	} else {
		msg = create_query_message (ean, data->barcode);
		g_signal_connect (G_OBJECT (msg), "got-body",
				  G_CALLBACK (got_body_query), data);
	}
	soup_session_queue_message (session, msg, NULL, NULL);
}
