/*
   Copyright (C) 2010 Bastien Nocera

   The Gnome Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   The Gnome Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with the Gnome Library; see the file COPYING.LIB.  If not,
   write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301  USA.

   Authors: Bastien Nocera <hadess@hadess.net>

 */

#ifndef DISCIDENT_EAN_RL_GLIB_H
#define DISCIDENT_EAN_RL_GLIB_H

#include <glib.h>
#include "discident-ean-private-glib.h"

G_BEGIN_DECLS

gboolean discident_ean_rl_lookup_sync (DiscidentEan *ean,
				       const char   *barcode,
				       char        **title,
				       char        **img_url,
				       GError      **error);

void discident_ean_rl_lookup (DiscidentEan        *ean,
			      SoupSession         *session,
			      QueryData           *data);

G_END_DECLS

#endif /* DISCIDENT_EAN_RL_GLIB_H */
