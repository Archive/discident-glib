/*
   Copyright (C) 2010 Bastien Nocera

   The Gnome Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   The Gnome Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with the Gnome Library; see the file COPYING.LIB.  If not,
   write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301  USA.

   Authors: Bastien Nocera <hadess@hadess.net>

 */

#ifndef DISCIDENT_ERROR_H
#define DISCIDENT_ERROR_H

#include <glib.h>

G_BEGIN_DECLS

/**
 * DiscidentError:
 * @DISCIDENT_ERROR_PARSE: An error occured parsing the response from the web service.
 * @DISCIDENT_ERROR_EMPTY_RESPONSE: No answers from the web service.
 * @DISCIDENT_ERROR_MISSING_PARAMETERS: Parameters are missing to use that web service.
 *
 * Error codes returned by discident-glib functions.
 **/
typedef enum {
	DISCIDENT_ERROR_PARSE,
	DISCIDENT_ERROR_EMPTY_RESPONSE,
	DISCIDENT_ERROR_MISSING_PARAMETERS
} DiscidentError;

/**
 * DISCIDENT_ERROR:
 *
 * Error domain for discident-glib. Errors from this domain will be from
 * the #DiscidentError enumeration.
 * See #GError for more information on error domains.
 **/

#define DISCIDENT_ERROR (discident_error_quark ())

GQuark discident_error_quark (void);

G_END_DECLS

#endif /* DISCIDENT_ERROR_H */
