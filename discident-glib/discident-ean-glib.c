/*
   Copyright (C) 2010 Bastien Nocera

   The Gnome Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   The Gnome Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with the Gnome Library; see the file COPYING.LIB.  If not,
   write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301  USA.

   Authors: Bastien Nocera <hadess@hadess.net>

 */

#include <string.h>

#include <libsoup/soup.h>
#include <glib/gprintf.h>
#include <libxml/tree.h>
#include <libxml/parser.h>

#include "discident-ean-glib.h"
#include "discident-error.h"
#include "discident-ean-amz-glib.h"
#include "discident-ean-rl-glib.h"
#include "discident-ean-private-glib.h"

/**
 * SECTION:discident-ean-glib
 * @short_description: Barcode lookup functions
 * @include: discident-glib/discident-ean-glib.h
 *
 * Contains functions to lookup title and thumbnails from barcodes.
 **/

G_DEFINE_TYPE (DiscidentEan, discident_ean, G_TYPE_OBJECT)

enum {
	PROP_0,
	PROP_SERVICE,
	PROP_PARAMETERS
};

const char *rl_services[] = {
	"rl-en_US",
	"rl-en_GB",
	"rl-fr_FR",
	"rl-de_DE",
	"rl-es_ES",
	"rl-it_IT",
	"amz-ca",
	"amz-cn",
	"amz-co.uk",
	"amz-com",
	"amz-de",
	"amz-es",
	"amz-fr",
	"amz-it",
	"amz-jp"
};

static gboolean
service_is_valid (const char *service)
{
	guint i;

	for (i = 0; i < G_N_ELEMENTS (rl_services); i++)
		if (g_strcmp0 (service, rl_services[i]) == 0)
			return TRUE;

	return FALSE;
}

static gboolean
parameters_are_valid (DiscidentEan  *ean,
		      GError       **error)
{
	if (ean->priv->access_key != NULL &&
	    ean->priv->private_key != NULL &&
	    ean->priv->associate_tag != NULL)
		return TRUE;

	if (ean->priv->access_key == NULL) {
		g_set_error (error, DISCIDENT_ERROR, DISCIDENT_ERROR_MISSING_PARAMETERS,
			     "Access key is missing to access this web service");
	} else if (ean->priv->private_key == NULL) {
		g_set_error (error, DISCIDENT_ERROR, DISCIDENT_ERROR_MISSING_PARAMETERS,
			     "Private key is missing to access this web service");
	} else if (ean->priv->associate_tag == NULL) {
		g_set_error (error, DISCIDENT_ERROR, DISCIDENT_ERROR_MISSING_PARAMETERS,
			     "Associate tag is missing to access this web service");
	}

	return FALSE;
}

static void
discident_ean_set_parameters (DiscidentEan *ean,
			      GHashTable   *parameters)
{
	if (parameters == NULL)
		return;
	ean->priv->access_key = g_strdup (g_hash_table_lookup (parameters, DISCIDENT_PARAM_ACCESS_KEY));
	ean->priv->private_key = g_strdup (g_hash_table_lookup (parameters, DISCIDENT_PARAM_PRIVATE_KEY));
	ean->priv->associate_tag = g_strdup (g_hash_table_lookup (parameters, DISCIDENT_PARAM_ASSOCIATE_TAG));
}

static void
discident_ean_set_property (GObject           *object,
			    guint              property_id,
			    const GValue      *value,
			    GParamSpec        *pspec)
{
	DiscidentEan *ean = (DiscidentEan *) object;

	switch (property_id) {
	case PROP_SERVICE:
		if (service_is_valid (g_value_get_string (value)))
			ean->priv->service = g_value_dup_string (value);
		else
			g_warning ("Invalid service '%s'", g_value_get_string (value));
		break;
	case PROP_PARAMETERS:
		discident_ean_set_parameters (ean, g_value_get_boxed (value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static void
discident_ean_get_property (GObject           *object,
			    guint              property_id,
			    GValue            *value,
			    GParamSpec        *pspec)
{
	DiscidentEan *ean = (DiscidentEan *) object;

	switch (property_id) {
	case PROP_SERVICE:
		g_value_set_string (value, ean->priv->service);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static void
discident_ean_finalize (GObject *object)
{
	DiscidentEan *ean;

	ean = DISCIDENT_EAN (object);

	g_clear_pointer (&ean->priv->server, g_free);
	g_clear_pointer (&ean->priv->message, g_free);

	g_clear_pointer (&ean->priv->access_key, g_free);
	g_clear_pointer (&ean->priv->private_key, g_free);
	g_clear_pointer (&ean->priv->associate_tag, g_free);

	G_OBJECT_CLASS (discident_ean_parent_class)->finalize (object);
}

static void
discident_ean_class_init (DiscidentEanClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->finalize = discident_ean_finalize;
        object_class->set_property = discident_ean_set_property;
        object_class->get_property = discident_ean_get_property;

	g_type_class_add_private (klass, sizeof (DiscidentEanPrivate));

        g_object_class_install_property (object_class,
                                         PROP_SERVICE,
                                         g_param_spec_string ("service",
                                                              "service name",
                                                              "service identifier",
                                                              "rl-en_US",
                                                              G_PARAM_CONSTRUCT | G_PARAM_READWRITE));
        g_object_class_install_property (object_class,
                                         PROP_PARAMETERS,
                                         g_param_spec_boxed ("parameters",
                                                             "service parameters",
                                                             "service parameters",
                                                             G_TYPE_HASH_TABLE,
                                                             G_PARAM_CONSTRUCT | G_PARAM_WRITABLE));
}

static void
discident_ean_init (DiscidentEan *ean)
{
	ean->priv = G_TYPE_INSTANCE_GET_PRIVATE ((ean), DISCIDENT_TYPE_EAN, DiscidentEanPrivate);
	ean->priv->enabled = TRUE;
	ean->priv->login_done = FALSE;
}

/**
 * discident_ean_lookup_sync:
 * @ean: a #DiscidentEan object representing a query
 * @barcode: a string representing the barcode to lookup
 * @title: (out): the return value for the title, or %NULL
 * @img_url: (out): the return value for the title, or %NULL
 * @error: a #GError
 *
 * Looks up the barcode with EAN @barcode, and sets @title
 * and @img_url appropriately if found.
 *
 * Returns: %TRUE on success. %FALSE on failure with @error set.
 **/
gboolean
discident_ean_lookup_sync (DiscidentEan *ean,
			   const char   *barcode,
			   char        **title,
			   char        **img_url,
			   GError      **error)
{
	g_return_val_if_fail (ean->priv->service != NULL, FALSE);
	g_return_val_if_fail (barcode != NULL, FALSE);

	if (img_url)
		*img_url = NULL;
	if (title)
		*title = NULL;

	if (g_str_has_prefix (ean->priv->service, "rl-")) {
		return discident_ean_rl_lookup_sync (ean,
						     barcode,
						     title,
						     img_url,
						     error);
	} else if (g_str_has_prefix (ean->priv->service, "amz-")) {
		if (!parameters_are_valid (ean, error))
			return FALSE;
		return discident_ean_amz_lookup_sync (ean,
						      barcode,
						      title,
						      img_url,
						      error);
	}

	g_assert_not_reached ();
}

void
free_query_data (QueryData *data)
{
	g_object_unref (data->ean);
	g_object_unref (data->session);
	g_free (data->barcode);
	g_free (data);
}

/**
 * discident_ean_lookup:
 * @ean: a #DiscidentEan object.
 * @barcode: the EAN barcode to lookup
 * @cancellable: optional #GCancellable object, %NULL to ignore.
 * @callback: a #GAsyncReadyCallback to call when the request is satisfied
 * @user_data: the data to pass to callback function
 *
 * Asynchronously gets the title and image URL for an EAN barcode
 * using a web service.
 *
 * When the operation is finished, @callback will be called. You can then call
 * discident_ean_lookup_finish() to get the result of the operation.
 **/
void
discident_ean_lookup (DiscidentEan        *ean,
		      const char          *barcode,
		      GCancellable        *cancellable,
		      GAsyncReadyCallback  callback,
		      gpointer             user_data)
{
	GSimpleAsyncResult *simple;
	SoupSession *session;
	QueryData *data;
	GError *error = NULL;

	simple = g_simple_async_result_new (G_OBJECT (ean),
					    callback,
					    user_data,
					    discident_ean_lookup);
	g_object_set_data_full (G_OBJECT (simple), "barcode", g_strdup (barcode), g_free);

	session = soup_session_new ();

	data = g_new0 (QueryData, 1);
	data->ean = g_object_ref (ean);
	data->session = session;
	data->barcode = g_strdup (barcode);
	data->simple = simple;

	if (g_str_has_prefix (ean->priv->service, "rl-")) {
		discident_ean_rl_lookup (ean, session, data);
	} else if (g_str_has_prefix (ean->priv->service, "amz-")) {
		if (!parameters_are_valid (ean, &error)) {
			g_simple_async_result_take_error (simple, error);
			g_simple_async_result_complete_in_idle (simple);
			return;
		}
		discident_ean_amz_lookup (ean, session, data);
	}
}

/**
 * discident_ean_lookup_finish:
 * @ean: a #DiscidentEan object
 * @res: a #GAsyncResult.
 * @img_url: (out): the return location for the image URL, or %NULL to ignore.
 * @error: a #GError.
 *
 * Finishes an EAN lookup operation. See discident_ean_lookup().
 *
 * Returns: a string containing the description of the looked up
 * barcode, or %NULL in case of errors.
 * Free the returned string with g_free() when done.
 **/
char *
discident_ean_lookup_finish (DiscidentEan  *ean,
			     GAsyncResult  *res,
			     char         **img_url,
			     GError       **error)
{
	GSimpleAsyncResult *simple = G_SIMPLE_ASYNC_RESULT (res);
	char *ret;

	g_warn_if_fail (g_simple_async_result_get_source_tag (simple) == discident_ean_lookup);

	ret = NULL;

	if (g_simple_async_result_propagate_error (simple, error))
		goto out;

	ret = g_simple_async_result_get_op_res_gpointer (simple);
	if (img_url != NULL)
		*img_url = g_strdup (g_object_get_data (G_OBJECT (res), "image-url"));

out:
	return ret;
}

/**
 * discident_ean_lookup_get_barcode:
 * @ean: a #DiscidentEan object
 * @res: a #GAsyncResult.
 *
 * Returns the barcode used for the EAN lookup operation.
 * See discident_ean_lookup().
 *
 * Returns: a string containing the looked up barcode, or
 * %NULL in case of errors.
 * Free the returned string with g_free() when done.
 **/
char *
discident_ean_lookup_get_barcode (DiscidentEan  *ean,
				  GAsyncResult  *res)
{
	GSimpleAsyncResult *simple;

	g_return_val_if_fail (res != NULL, NULL);
	simple = G_SIMPLE_ASYNC_RESULT (res);
	g_warn_if_fail (g_simple_async_result_get_source_tag (simple) == discident_ean_lookup);

	return g_strdup (g_object_get_data (G_OBJECT (res), "barcode"));
}

/**
 * discident_ean_new:
 * @service: The identifier for the service, or %NULL to use
 * the default service.
 *
 * Create a new #DiscidentEan object to lookup barcodes with.
 *
 * Returns: a new #DiscidentEan object.
 **/
DiscidentEan *
discident_ean_new (const char *service)
{
	g_return_val_if_fail (!service || service_is_valid (service), NULL);

	if (service)
		return g_object_new (DISCIDENT_TYPE_EAN, "service", service, NULL);

	return g_object_new (DISCIDENT_TYPE_EAN, NULL);
}

/**
 * discident_ean_new_full:
 * @service: The identifier for the service, or %NULL to use
 * the default service.
 * @parameters: a #GHashTable of string keys and string values. The keys
 * to use depend on the service used.
 *
 * Create a new #DiscidentEan object to lookup barcodes with.
 *
 * Returns: a new #DiscidentEan object.
 **/
DiscidentEan *
discident_ean_new_full (const char *service,
			GHashTable *parameters)
{
	g_return_val_if_fail (!service || service_is_valid (service), NULL);

	if (service)
		return g_object_new (DISCIDENT_TYPE_EAN, "service", service, "parameters", parameters, NULL);

	return g_object_new (DISCIDENT_TYPE_EAN, NULL);
}
