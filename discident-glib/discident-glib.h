/*
   Copyright (C) 2010 Bastien Nocera

   The Gnome Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   The Gnome Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with the Gnome Library; see the file COPYING.LIB.  If not,
   write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301  USA.

   Authors: Bastien Nocera <hadess@hadess.net>

 */

#ifndef DISCIDENT_GLIB_H
#define DISCIDENT_GLIB_H

#include <glib.h>
#include <gio/gio.h>
#include <discident-error.h>

G_BEGIN_DECLS

char *discident_get_gtin_file_sync   (GFile   *directory,
				      GError **error);
void  discident_get_gtin_file        (GFile               *directory,
				      GCancellable        *cancellable,
				      GAsyncReadyCallback  callback,
				      gpointer             user_data);
char *discident_get_gtin_file_finish (GFile               *directory,
				      GAsyncResult        *res,
				      GError             **error);

char *discident_get_title_sync        (GFile   *directory,
				       GError **error);
void  discident_get_title_file        (GFile               *directory,
				       GCancellable        *cancellable,
				       GAsyncReadyCallback  callback,
				       gpointer             user_data);
char *discident_get_title_file_finish (GFile               *directory,
				       GAsyncResult        *res,
				       GError             **error);

G_END_DECLS

#endif /* DISCIDENT_GLIB_H */
