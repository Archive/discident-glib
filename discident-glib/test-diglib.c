
#include "config.h"

#include <locale.h>
#include <glib/gi18n.h>
#include <glib.h>
#include <gio/gio.h>
#include <discident-glib.h>
#include <discident-ean-glib.h>
#include <discident-glib-private.h>
#include <discident-ean-private-glib.h>

static gboolean option_async = FALSE;
static gboolean option_file_list = FALSE;
static gboolean option_lookup_ean = FALSE;
static char *ean_service = NULL;
static char *access_key = NULL;
static char *private_key = NULL;
static char *associate_tag = NULL;
static guint num_queries = 0;
static GMainLoop *loop = NULL;

static GHashTable *create_params (void);

static void
test_hash (void)
{
	/* From http://github.com/kastner/dvd_fingerprint/blob/master/dvd_fingerprint.rb */
	g_assert_cmpstr (generate_hash (":/VIDEO_TS/VIDEO_TS.BUP:12288:/VIDEO_TS/VIDEO_TS.IFO:12288:/VIDEO_TS/VIDEO_TS.VOB:58052608:/VIDEO_TS/VTS_01_0.BUP:98304:/VIDEO_TS/VTS_01_0.IFO:98304:/VIDEO_TS/VTS_01_0.VOB:75163648:/VIDEO_TS/VTS_01_1.VOB:1073598464:/VIDEO_TS/VTS_01_2.VOB:1073448960:/VIDEO_TS/VTS_01_3.VOB:1073741824:/VIDEO_TS/VTS_01_4.VOB:1073647616:/VIDEO_TS/VTS_01_5.VOB:835813376", -1), ==, "2075AB92-06CD-ED43-A753-2B75627BE844");
}

static void
test_file_list (void)
{
	guint i;
	struct {
		const char *dir;
		const char *gtin;
		const char *file_list;
	} directories[] = {
		{ "Alexander Nevsky", "3809DA3F-8323-2E48-70C6-861B34968674", ":/VIDEO_TS/VIDEO_TS.BUP:18432:/VIDEO_TS/VIDEO_TS.IFO:18432:/VIDEO_TS/VIDEO_TS.VOB:178176:/VIDEO_TS/VTS_01_0.BUP:24576:/VIDEO_TS/VTS_01_0.IFO:24576:/VIDEO_TS/VTS_01_0.VOB:104853504:/VIDEO_TS/VTS_01_1.VOB:13611008:/VIDEO_TS/VTS_02_0.BUP:18432:/VIDEO_TS/VTS_02_0.IFO:18432:/VIDEO_TS/VTS_02_0.VOB:178176:/VIDEO_TS/VTS_02_1.VOB:25085952:/VIDEO_TS/VTS_03_0.BUP:18432:/VIDEO_TS/VTS_03_0.IFO:18432:/VIDEO_TS/VTS_03_0.VOB:178176:/VIDEO_TS/VTS_03_1.VOB:9498624:/VIDEO_TS/VTS_04_0.BUP:18432:/VIDEO_TS/VTS_04_0.IFO:18432:/VIDEO_TS/VTS_04_0.VOB:178176:/VIDEO_TS/VTS_04_1.VOB:2146304:/VIDEO_TS/VTS_05_0.BUP:65536:/VIDEO_TS/VTS_05_0.IFO:65536:/VIDEO_TS/VTS_05_0.VOB:178176:/VIDEO_TS/VTS_05_1.VOB:1073565696:/VIDEO_TS/VTS_05_2.VOB:1073565696:/VIDEO_TS/VTS_05_3.VOB:1073565696:/VIDEO_TS/VTS_05_4.VOB:1073565696:/VIDEO_TS/VTS_05_5.VOB:206231552" },
		{ "Être et Avoir", "D2740096-2507-09FC-EE0F-D577CBF98536", ":/VIDEO_TS/VIDEO_TS.BUP:22528:/VIDEO_TS/VIDEO_TS.IFO:22528:/VIDEO_TS/VIDEO_TS.VOB:157696:/VIDEO_TS/VTS_01_0.BUP:24576:/VIDEO_TS/VTS_01_0.IFO:24576:/VIDEO_TS/VTS_01_0.VOB:58773504:/VIDEO_TS/VTS_01_1.VOB:9805824:/VIDEO_TS/VTS_02_0.BUP:18432:/VIDEO_TS/VTS_02_0.IFO:18432:/VIDEO_TS/VTS_02_0.VOB:157696:/VIDEO_TS/VTS_02_1.VOB:3958784:/VIDEO_TS/VTS_03_0.BUP:18432:/VIDEO_TS/VTS_03_0.IFO:18432:/VIDEO_TS/VTS_03_0.VOB:157696:/VIDEO_TS/VTS_03_1.VOB:80705536:/VIDEO_TS/VTS_04_0.BUP:18432:/VIDEO_TS/VTS_04_0.IFO:18432:/VIDEO_TS/VTS_04_0.VOB:157696:/VIDEO_TS/VTS_04_1.VOB:99594240:/VIDEO_TS/VTS_05_0.BUP:18432:/VIDEO_TS/VTS_05_0.IFO:18432:/VIDEO_TS/VTS_05_0.VOB:157696:/VIDEO_TS/VTS_05_1.VOB:71680000:/VIDEO_TS/VTS_06_0.BUP:18432:/VIDEO_TS/VTS_06_0.IFO:18432:/VIDEO_TS/VTS_06_0.VOB:157696:/VIDEO_TS/VTS_06_1.VOB:148228096:/VIDEO_TS/VTS_07_0.BUP:18432:/VIDEO_TS/VTS_07_0.IFO:18432:/VIDEO_TS/VTS_07_0.VOB:157696:/VIDEO_TS/VTS_07_1.VOB:87965696:/VIDEO_TS/VTS_08_0.BUP:26624:/VIDEO_TS/VTS_08_0.IFO:26624:/VIDEO_TS/VTS_08_0.VOB:157696:/VIDEO_TS/VTS_08_1.VOB:426055680:/VIDEO_TS/VTS_09_0.BUP:73728:/VIDEO_TS/VTS_09_0.IFO:73728:/VIDEO_TS/VTS_09_0.VOB:157696:/VIDEO_TS/VTS_09_1.VOB:1073565696:/VIDEO_TS/VTS_09_2.VOB:1073565696:/VIDEO_TS/VTS_09_3.VOB:932294656" },
		{ "The Kite Runner", "5F03F0D5-6AB6-FDB4-43AE-825693898CFF", ":/VIDEO_TS/VIDEO_TS.BUP:20480:/VIDEO_TS/VIDEO_TS.IFO:20480:/VIDEO_TS/VIDEO_TS.VOB:935936:/VIDEO_TS/VTS_01_0.BUP:14336:/VIDEO_TS/VTS_01_0.IFO:14336:/VIDEO_TS/VTS_01_1.VOB:471040:/VIDEO_TS/VTS_02_0.BUP:24576:/VIDEO_TS/VTS_02_0.IFO:24576:/VIDEO_TS/VTS_02_0.VOB:13301760:/VIDEO_TS/VTS_02_1.VOB:423208960:/VIDEO_TS/VTS_03_0.BUP:18432:/VIDEO_TS/VTS_03_0.IFO:18432:/VIDEO_TS/VTS_03_0.VOB:229376:/VIDEO_TS/VTS_03_1.VOB:75370496:/VIDEO_TS/VTS_04_0.BUP:40960:/VIDEO_TS/VTS_04_0.IFO:40960:/VIDEO_TS/VTS_04_1.VOB:1073709056:/VIDEO_TS/VTS_04_2.VOB:327958528:/VIDEO_TS/VTS_05_0.BUP:90112:/VIDEO_TS/VTS_05_0.IFO:90112:/VIDEO_TS/VTS_05_0.VOB:58671104:/VIDEO_TS/VTS_05_1.VOB:1073709056:/VIDEO_TS/VTS_05_2.VOB:1073709056:/VIDEO_TS/VTS_05_3.VOB:1073709056:/VIDEO_TS/VTS_05_4.VOB:1073709056:/VIDEO_TS/VTS_05_5.VOB:884168704:/VIDEO_TS/VTS_06_0.BUP:14336:/VIDEO_TS/VTS_06_0.IFO:14336:/VIDEO_TS/VTS_06_1.VOB:43491328:/VIDEO_TS/VTS_07_0.BUP:18432:/VIDEO_TS/VTS_07_0.IFO:18432:/VIDEO_TS/VTS_07_0.VOB:18966528:/VIDEO_TS/VTS_07_1.VOB:6029312" },
		{ "The Straight Story", "4C059E8A-37E4-493E-6272-F9A713DB5AA9", ":/VIDEO_TS/VIDEO_TS.BUP:22528:/VIDEO_TS/VIDEO_TS.IFO:22528:/VIDEO_TS/VIDEO_TS.VOB:157696:/VIDEO_TS/VTS_01_0.BUP:34816:/VIDEO_TS/VTS_01_0.IFO:34816:/VIDEO_TS/VTS_01_0.VOB:255559680:/VIDEO_TS/VTS_01_1.VOB:997376:/VIDEO_TS/VTS_02_0.BUP:65536:/VIDEO_TS/VTS_02_0.IFO:65536:/VIDEO_TS/VTS_02_0.VOB:157696:/VIDEO_TS/VTS_02_1.VOB:1073565696:/VIDEO_TS/VTS_02_2.VOB:1073565696:/VIDEO_TS/VTS_02_3.VOB:1073565696:/VIDEO_TS/VTS_02_4.VOB:1073565696:/VIDEO_TS/VTS_02_5.VOB:1073565696:/VIDEO_TS/VTS_02_6.VOB:1073565696:/VIDEO_TS/VTS_02_7.VOB:376975360:/VIDEO_TS/VTS_03_0.BUP:18432:/VIDEO_TS/VTS_03_0.IFO:18432:/VIDEO_TS/VTS_03_0.VOB:157696:/VIDEO_TS/VTS_03_1.VOB:6483968:/VIDEO_TS/VTS_04_0.BUP:18432:/VIDEO_TS/VTS_04_0.IFO:18432:/VIDEO_TS/VTS_04_0.VOB:157696:/VIDEO_TS/VTS_04_1.VOB:5986304:/VIDEO_TS/VTS_05_0.BUP:18432:/VIDEO_TS/VTS_05_0.IFO:18432:/VIDEO_TS/VTS_05_0.VOB:157696:/VIDEO_TS/VTS_05_1.VOB:15337472:/VIDEO_TS/VTS_06_0.BUP:18432:/VIDEO_TS/VTS_06_0.IFO:18432:/VIDEO_TS/VTS_06_0.VOB:157696:/VIDEO_TS/VTS_06_1.VOB:67874816:/VIDEO_TS/VTS_07_0.BUP:18432:/VIDEO_TS/VTS_07_0.IFO:18432:/VIDEO_TS/VTS_07_0.VOB:157696:/VIDEO_TS/VTS_07_1.VOB:123170816:/VIDEO_TS/VTS_08_0.BUP:18432:/VIDEO_TS/VTS_08_0.IFO:18432:/VIDEO_TS/VTS_08_0.VOB:157696:/VIDEO_TS/VTS_08_1.VOB:9367552" },
		{ "The Woodsman", "724DAC2A-6BB7-21E8-2F85-BCD0A56ED995", ":/VIDEO_TS/VIDEO_TS.BUP:24576:/VIDEO_TS/VIDEO_TS.IFO:24576:/VIDEO_TS/VIDEO_TS.VOB:157696:/VIDEO_TS/VTS_01_0.BUP:92160:/VIDEO_TS/VTS_01_0.IFO:92160:/VIDEO_TS/VTS_01_0.VOB:248268800:/VIDEO_TS/VTS_01_1.VOB:1073565696:/VIDEO_TS/VTS_01_2.VOB:1073565696:/VIDEO_TS/VTS_01_3.VOB:1073565696:/VIDEO_TS/VTS_01_4.VOB:1073565696:/VIDEO_TS/VTS_01_5.VOB:773203968:/VIDEO_TS/VTS_02_0.BUP:30720:/VIDEO_TS/VTS_02_0.IFO:30720:/VIDEO_TS/VTS_02_0.VOB:157696:/VIDEO_TS/VTS_02_1.VOB:743315456:/VIDEO_TS/VTS_03_0.BUP:18432:/VIDEO_TS/VTS_03_0.IFO:18432:/VIDEO_TS/VTS_03_0.VOB:157696:/VIDEO_TS/VTS_03_1.VOB:122218496:/VIDEO_TS/VTS_04_0.BUP:26624:/VIDEO_TS/VTS_04_0.IFO:26624:/VIDEO_TS/VTS_04_0.VOB:157696:/VIDEO_TS/VTS_04_1.VOB:625369088:/VIDEO_TS/VTS_05_0.BUP:26624:/VIDEO_TS/VTS_05_0.IFO:26624:/VIDEO_TS/VTS_05_0.VOB:157696:/VIDEO_TS/VTS_05_1.VOB:592257024:/VIDEO_TS/VTS_06_0.BUP:20480:/VIDEO_TS/VTS_06_0.IFO:20480:/VIDEO_TS/VTS_06_0.VOB:157696:/VIDEO_TS/VTS_06_1.VOB:200243200:/VIDEO_TS/VTS_07_0.BUP:18432:/VIDEO_TS/VTS_07_0.IFO:18432:/VIDEO_TS/VTS_07_0.VOB:157696:/VIDEO_TS/VTS_07_1.VOB:75008000:/VIDEO_TS/VTS_08_0.BUP:18432:/VIDEO_TS/VTS_08_0.IFO:18432:/VIDEO_TS/VTS_08_0.VOB:157696:/VIDEO_TS/VTS_08_1.VOB:103041024:/VIDEO_TS/VTS_09_0.BUP:18432:/VIDEO_TS/VTS_09_0.IFO:18432:/VIDEO_TS/VTS_09_0.VOB:157696:/VIDEO_TS/VTS_09_1.VOB:146837504:/VIDEO_TS/VTS_10_0.BUP:18432:/VIDEO_TS/VTS_10_0.IFO:18432:/VIDEO_TS/VTS_10_0.VOB:157696:/VIDEO_TS/VTS_10_1.VOB:87701504:/VIDEO_TS/VTS_11_0.BUP:18432:/VIDEO_TS/VTS_11_0.IFO:18432:/VIDEO_TS/VTS_11_0.VOB:157696:/VIDEO_TS/VTS_11_1.VOB:4814848:/VIDEO_TS/VTS_12_0.BUP:18432:/VIDEO_TS/VTS_12_0.IFO:18432:/VIDEO_TS/VTS_12_0.VOB:157696:/VIDEO_TS/VTS_12_1.VOB:5820416" },
	};

	if (g_strcmp0 (g_get_user_name (), "hadess") != 0 ||
	    g_file_test ("/home/data/DVDs/", G_FILE_TEST_IS_DIR) == FALSE) {
		g_test_skip ("DVDs directory is missing");
		return;
	}

	for (i = 0; i < G_N_ELEMENTS (directories); i++) {
		GFile *dir;
		char *filename;

		filename = g_build_filename ("/home/data/DVDs/", directories[i].dir, NULL);
		dir = g_file_new_for_path (filename);
		g_free (filename);

		g_assert_cmpstr (discident_get_gtin_file_sync (dir, NULL), ==, directories[i].gtin);

		g_object_set_data (G_OBJECT (dir), "wants-file-list", GINT_TO_POINTER (TRUE));

		g_assert_cmpstr (discident_get_gtin_file_sync (dir, NULL), ==, directories[i].file_list);

		g_object_unref (dir);
	}
}

static void
test_json (void)
{
	guint i;
	struct {
		const char *gtin;
		const char *title;
	} dvds[] = {
		{ "3809DA3F-8323-2E48-70C6-861B34968674", "Sergei Eisenstein: Alexander Nevsky (1938)" },
		{ "5F03F0D5-6AB6-FDB4-43AE-825693898CFF", "The Kite Runner" },
		{ "4C059E8A-37E4-493E-6272-F9A713DB5AA9", "The Straight Story" },
		{ "724DAC2A-6BB7-21E8-2F85-BCD0A56ED995", "Woodsman, The (2004)" },
		{ "D2740096-2507-09FC-EE0F-D577CBF98536", "(2003) Etre et Avoir" },
	};

	for (i = 0; i < G_N_ELEMENTS (dvds); i++) {
		g_assert_cmpstr (discident_get_title_for_gtin (dvds[i].gtin, NULL), ==, dvds[i].title);
	}
}

static void
test_rl_parse (void)
{
	char *response;
	gboolean ret;
	char *img_url, *title;

	g_file_get_contents (SRCDIR "/rl-response.xml",
			     &response,
			     NULL,
			     NULL);
	ret = _rl_parse_lookup_response (response,
					 &title,
					 &img_url);
	g_assert (ret);
	g_assert_cmpstr (title, ==, "2001 - A Space Odyssey (DVD 2001)");
	g_assert_cmpstr (img_url, ==, "http://i.ebayimg.com/02/!!eGJetgBGM~$(KGrHqR,!kwE1LC7oFIqBNVdzdgekQ~~_6.JPG?set_id=89040003C1");
}

static void
test_amz_parse (void)
{
	char *response;
	gboolean ret;
	char *img_url, *title;
	GError *error = NULL;

	g_file_get_contents (SRCDIR "/amz-response.xml",
			     &response,
			     NULL,
			     NULL);
	ret = _amz_parse_lookup_response (response,
					 &title,
					 &img_url,
					 NULL);
	g_assert (ret);
	g_assert_cmpstr (title, ==, "JavaScript Pocket Reference (Pocket Reference (O'Reilly))");
	g_assert_cmpstr (img_url, ==, "http://ecx.images-amazon.com/images/I/51UL7NX3-6L.jpg");
	g_free (title);
	g_free (img_url);

	g_file_get_contents (SRCDIR "/amz-response-error.xml",
			     &response,
			     NULL,
			     NULL);
	ret = _amz_parse_lookup_response (response,
					  &title,
					  &img_url,
					  &error);
	g_assert_false (ret);
	g_assert_error (error, DISCIDENT_ERROR, DISCIDENT_ERROR_PARSE);
	g_assert_cmpstr (error->message, ==, "2253101125 is not a valid value for ItemId. Please change this value and retry your request.");
	g_clear_error (&error);

	ret = _amz_parse_lookup_response (NULL,
					  &title,
					  &img_url,
					  &error);
	g_assert_false (ret);
	g_assert_error (error, DISCIDENT_ERROR, DISCIDENT_ERROR_PARSE);
	g_assert_cmpstr (error->message, ==, "Empty response");
	g_clear_error (&error);

	ret = _amz_parse_lookup_response ("<Item><Item>",
					  &title,
					  &img_url,
					  &error);
	g_assert_false (ret);
	g_assert_error (error, DISCIDENT_ERROR, DISCIDENT_ERROR_PARSE);
	g_assert_cmpstr (error->message, ==, "Could not parse XML in response");
	g_clear_error (&error);
}

static void
test_amz1 (void)
{
	char *ret;
	const char *steps[] = {
		"http://webservices.amazon.com/onca/xml?Service=AWSECommerceService&AWSAccessKeyId=AKIAIOSFODNN7EXAMPLE&Operation=ItemLookup&ItemId=0679722769&ResponseGroup=ItemAttributes,Offers,Images,Reviews&Version=2009-01-06&Timestamp=2009-01-01T12:00:00Z",
		"http://webservices.amazon.com/onca/xml?Service=AWSECommerceService&AWSAccessKeyId=AKIAIOSFODNN7EXAMPLE&Operation=ItemLookup&ItemId=0679722769&ResponseGroup=ItemAttributes%2COffers%2CImages%2CReviews&Version=2009-01-06&Timestamp=2009-01-01T12%3A00%3A00Z",
		NULL,
		NULL,
		"AWSAccessKeyId=AKIAIOSFODNN7EXAMPLE&ItemId=0679722769&Operation=ItemLookup&ResponseGroup=ItemAttributes%2COffers%2CImages%2CReviews&Service=AWSECommerceService&Timestamp=2009-01-01T12%3A00%3A00Z&Version=2009-01-06",
		NULL,
		"GET\nwebservices.amazon.com\n/onca/xml\nAWSAccessKeyId=AKIAIOSFODNN7EXAMPLE&ItemId=0679722769&Operation=ItemLookup&ResponseGroup=ItemAttributes%2COffers%2CImages%2CReviews&Service=AWSECommerceService&Timestamp=2009-01-01T12%3A00%3A00Z&Version=2009-01-06",
		"M/y0+EAFFGaUAp4bWv/WEuXYah99pVsxvqtAuC8YN7I=",
		"M%2Fy0%2BEAFFGaUAp4bWv%2FWEuXYah99pVsxvqtAuC8YN7I%3D",
		/* Note: this has been modified to reorder the parameters as
		 * per other tests */
		"http://webservices.amazon.com/onca/xml?AWSAccessKeyId=AKIAIOSFODNN7EXAMPLE&ItemId=0679722769&Operation=ItemLookup&ResponseGroup=ItemAttributes%2COffers%2CImages%2CReviews&Service=AWSECommerceService&Signature=M%2Fy0%2BEAFFGaUAp4bWv%2FWEuXYah99pVsxvqtAuC8YN7I%3D&Timestamp=2009-01-01T12%3A00%3A00Z&Version=2009-01-06"
	};

	ret = _sign_query ("http://webservices.amazon.com/onca/xml?Service=AWSECommerceService&AWSAccessKeyId=AKIAIOSFODNN7EXAMPLE&Operation=ItemLookup&ItemId=0679722769&ResponseGroup=ItemAttributes,Offers,Images,Reviews&Version=2009-01-06", "2009-01-01T12:00:00Z", "1234567890", steps);
	g_free (ret);
}

static void
test_amz2 (void)
{
	char *ret;
	const char *steps[10];
	memset (steps, 0, sizeof(steps));
	steps[6] = "GET\necs.amazonaws.co.uk\n/onca/xml\nAWSAccessKeyId=AKIAIOSFODNN7EXAMPLE&Actor=Johnny%20Depp&AssociateTag=mytag-20&Operation=ItemSearch&ResponseGroup=ItemAttributes%2COffers%2CImages%2CReviews%2CVariations&SearchIndex=DVD&Service=AWSECommerceService&Sort=salesrank&Timestamp=2009-01-01T12%3A00%3A00Z&Version=2009-01-01";
//	steps[9] = "http://ecs.amazonaws.co.uk/onca/xml?AWSAccessKeyId=AKIAIOSFODNN7EXAMPLE&Actor=Johnny%20Depp&AssociateTag=mytag-20&Operation=ItemSearch&ResponseGroup=ItemAttributes%2COffers%2CImages%2CReviews%2CVariations&SearchIndex=DVD&Service=AWSECommerceService&Signature=TuM6E5L9u%2FuNqOX09ET03BXVmHLVFfJIna5cxXuHxiU%3D&Sort=salesrank&Timestamp=2009-01-01T12%3A00%3A00Z&Version=2009-01-01";

	ret = _sign_query ("http://ecs.amazonaws.co.uk/onca/xml?Service=AWSECommerceService&AWSAccessKeyId=AKIAIOSFODNN7EXAMPLE&Operation=ItemSearch&Actor=Johnny%20Depp&ResponseGroup=ItemAttributes,Offers,Images,Reviews,Variations&Version=2009-01-01&SearchIndex=DVD&Sort=salesrank&AssociateTag=mytag-20", "2009-01-01T12:00:00Z", "1234567890", steps);
	g_free (ret);
}

static void
test_amz3 (void)
{
	char *ret;
	const char *steps[10];
	memset (steps, 0, sizeof(steps));
	steps[6] = "GET\nwebservices.amazon.com\n/onca/xml\nAWSAccessKeyId=AKIAIOSFODNN7EXAMPLE&AssociateTag=mytag-20&Item.1.OfferListingId=j8ejq9wxDfSYWf2OCp6XQGDsVrWhl08GSQ9m5j%2Be8MS449BN1XGUC3DfU5Zw4nt%2FFBt87cspLow1QXzfvZpvzg%3D%3D&Item.1.Quantity=3&Operation=CartCreate&Service=AWSECommerceService&Timestamp=2009-01-01T12%3A00%3A00Z&Version=2009-01-01";
//	steps[9] = "http://webservices.amazon.com/onca/xml?AWSAccessKeyId=AKIAIOSFODNN7EXAMPLE&AssociateTag=mytag-20&Item.1.OfferListingId=j8ejq9wxDfSYWf2OCp6XQGDsVrWhl08GSQ9m5j%2Be8MS449BN1XGUC3DfU5Zw4nt%2FFBt87cspLow1QXzfvZpvzg%3D%3D&Item.1.Quantity=3&Operation=CartCreate&Service=AWSECommerceService&Signature=cF3UtjbJb1%2BxDh387C%2FEmS1BCtS%2FZ01taykBCGemvUU%3D&Timestamp=2009-01-01T12%3A00%3A00Z&Version=2009-01-01";

	ret = _sign_query ("http://webservices.amazon.com/onca/xml?Service=AWSECommerceService&AWSAccessKeyId=AKIAIOSFODNN7EXAMPLE&Operation=CartCreate&Version=2009-01-01&Item.1.OfferListingId=j8ejq9wxDfSYWf2OCp6XQGDsVrWhl08GSQ9m5j%2Be8MS449BN1XGUC3DfU5Zw4nt%2FFBt87cspLow1QXzfvZpvzg%3D%3D&Item.1.Quantity=3&AssociateTag=mytag-20", "2009-01-01T12:00:00Z", "1234567890", steps);
	g_free (ret);
}

static void
test_amz4 (void)
{
	char *ret;
	const char *steps[10];
	memset (steps, 0, sizeof(steps));
	steps[6] = "GET\nwebservices.amazon.com\n/onca/xml\nAWSAccessKeyId=AKIAIOSFODNN7EXAMPLE&AssociateTag=mytag-20&BrowseNodeId=465600&Operation=BrowseNodeLookup&ResponseGroup=BrowseNodeInfo%2CTopSellers%2CNewReleases%2CMostWishedFor%2CMostGifted&Service=AWSECommerceService&Timestamp=2009-01-01T12%3A00%3A00Z&Version=2009-01-01";
//	steps[9] = "http://webservices.amazon.com/onca/xml?AWSAccessKeyId=AKIAIOSFODNN7EXAMPLE&AssociateTag=mytag-20&BrowseNodeId=465600&Operation=BrowseNodeLookup&ResponseGroup=BrowseNodeInfo%2CTopSellers%2CNewReleases%2CMostWishedFor%2CMostGifted&Service=AWSECommerceService&Signature=kEXxAIqhh6eBhLhrVMz2gt3ocMaH%2FOBVPbjvc9TG8ao%3D&Timestamp=2009-01-01T12%3A00%3A00Z&Version=2009-01-01";

	ret = _sign_query ("http://webservices.amazon.com/onca/xml?Service=AWSECommerceService&AWSAccessKeyId=AKIAIOSFODNN7EXAMPLE&Operation=BrowseNodeLookup&Version=2009-01-01&BrowseNodeId=465600&AssociateTag=mytag-20&ResponseGroup=BrowseNodeInfo,TopSellers,NewReleases,MostWishedFor,MostGifted", "2009-01-01T12:00:00Z", "1234567890", steps);
	g_free (ret);
}

static void
test_amz5 (void)
{
	/* This is completely busted,
	 * there's a ";" that somebody typoed into
	 * the GET request example */
#if 0
	char *ret;
	const char *steps[10];
	memset (steps, 0, sizeof(steps));
	steps[6] = "GET\nwebservices.amazon.com\n/onca/xml\nAWSAccessKeyId=AKIAIOSFODNN7EXAMPLE;&AssociateTag=mytag-20&Condition=New&ItemId=B0011ZK6PC%2CB000NK8EWI&Merchant=Amazon&Operation=SimilarityLookup&ResponseGroup=Offers%2CItemAttributes&Service=AWSECommerceService&SimilarityType=Intersection&Timestamp=2009-01-01T12%3A00%3A00Z&Version=2009-01-01";
	steps[9] = "http://webservices.amazon.com/onca/xml?AWSAccessKeyId=AKIAIOSFODNN7EXAMPLE&AssociateTag=mytag-20&Condition=New&ItemId=B0011ZK6PC%2CB000NK8EWI&Merchant=Amazon&Operation=SimilarityLookup&ResponseGroup=Offers%2CItemAttributes&Service=AWSECommerceService&Signature=I2pbqxuS%2FmZK6Apwz0oLBxJn2wDL5n4kFQhgYWgLM7I%3D&SimilarityType=Intersection&Timestamp=2009-01-01T12%3A00%3A00Z&Version=2009-01-01";

	ret = _sign_query ("http://webservices.amazon.com/onca/xml?Service=AWSECommerceService&AWSAccessKeyId=AKIAIOSFODNN7EXAMPLE&Operation=SimilarityLookup&ItemId=B0011ZK6PC,B000NK8EWI&Version=2009-01-01&AssociateTag=mytag-20&ResponseGroup=Offers,ItemAttributes&SimilarityType=Intersection&Condition=New&Merchant=Amazon", "2009-01-01T12:00:00Z", "1234567890", steps);
	g_free (ret);
#endif
}

static void
test_ean (void)
{
	DiscidentEan *ean;
	char *title, *img_url;
	GError *error = NULL;
	gboolean ret;

	ean = discident_ean_new (NULL);

	/* The Little Book of Stress: Calm is for Wimps, Get Real, Get Stressed */
	ret = discident_ean_lookup_sync (ean, "9780091865856", &title, &img_url, &error);
	if (ret == FALSE) {
		g_test_message ("discident_ean_lookup_sync() failed with: %s", error->message);
		g_error_free (error);
		g_assert_not_reached ();
	}
	g_assert_cmpstr ("The Little Book of Stress: Calm Is for Wimps. Get Real. Get Stressed", ==, title);
	g_assert (img_url != NULL);
	g_free (title);
	g_free (img_url);
	g_object_unref (ean);
}

static void
test_ean_fail (void)
{
	DiscidentEan *ean;
	char *title, *img_url;
	GError *error = NULL;
	gboolean ret;

	ean = discident_ean_new (NULL);

	/* Ghost In The Shell, FR */
	ret = discident_ean_lookup_sync (ean, "3388330031138", &title, &img_url, &error);
	g_assert (ret == FALSE);
	g_assert_cmpstr ("No response for requested barcode", ==, error->message);
	g_error_free (error);
	g_free (title);
	g_free (img_url);
	g_object_unref (ean);

	ean = discident_ean_new ("rl-fr_FR");

	/* But works with the FR check */
	ret = discident_ean_lookup_sync (ean, "3388330031138", &title, NULL, &error);
	g_assert (ret == TRUE);
	g_assert_cmpstr ("GHOST IN THE SHELL (1995 DVD NON MUSICAL)", ==, title);
	g_free (title);
	g_object_unref (ean);
}

static void
test_ean_amz (void)
{
	GHashTable *params;
	DiscidentEan *ean;
	gboolean ret;
	char *title, *img_url;
	GError *error = NULL;

	params = create_params ();
	if (!params) {
		g_test_skip ("Missing Amazon configuration");
		return;
	}

	ean = discident_ean_new_full ("amz-fr", params);

	/* Ghost in the shell */
	ret = discident_ean_lookup_sync (ean, "3388330031138", &title, &img_url, &error);
	if (ret == FALSE)
		g_assert_no_error (error);
	g_assert_cmpstr ("Ghost in the shell", ==, title);
	g_assert (img_url != NULL);
	g_free (title);
	g_free (img_url);

	/* The Little Book of Stress: Calm is for Wimps, Get Real, Get Stressed */
	ret = discident_ean_lookup_sync (ean, "9780091865856", &title, &img_url, &error);
	if (ret == FALSE)
		g_assert_no_error (error);
	g_assert_cmpstr ("The Little Book Of Stress", ==, title);
	g_assert (img_url != NULL);
	g_free (title);
	g_free (img_url);

	g_object_unref (ean);
}

static void
discident_title_print (GObject *source_object,
		      GAsyncResult *res,
		      gpointer user_data)
{
	GFile *directory = G_FILE (source_object);
	GError *error = NULL;
	char *title;

	title = discident_get_title_file_finish (directory, res, &error);
	if (title == NULL) {
		g_message ("Could not get disc ID: %s", error->message);
		g_object_unref (directory);
		g_error_free (error);
		return;
	}
	g_print ("Title: %s\n", title);
	g_free (title);

	num_queries--;
	if (num_queries == 0)
		g_main_loop_quit (loop);
}

static void
discident_gtin_print (GObject *source_object,
		      GAsyncResult *res,
		      gpointer user_data)
{
	GFile *directory = G_FILE (source_object);
	GError *error = NULL;
	char *id;

	id = discident_get_gtin_file_finish (directory, res, &error);
	if (id == NULL) {
		g_message ("Could not get disc ID: %s", error->message);
		g_object_unref (directory);
		g_error_free (error);
		goto bail;
	}
	g_print ("%s\n", id);
	g_free (id);

bail:
	num_queries--;
	if (num_queries == 0)
		g_main_loop_quit (loop);
}

static char **uris = NULL;

static void
discident_ean_print (GObject *source_object,
		     GAsyncResult *res,
		     gpointer user_data)
{
	GError *error = NULL;
	char *title, *img_url, *barcode;

	barcode = discident_ean_lookup_get_barcode (DISCIDENT_EAN (source_object),
						    res);
	title = discident_ean_lookup_finish (DISCIDENT_EAN (source_object),
					     res,
					     &img_url,
					     &error);
	if (title == NULL) {
		g_message ("Could not query barcode '%s': %s", barcode, error->message);
		g_free (barcode);
		g_error_free (error);
		goto bail;
	}

	g_print ("EAN: %s\n", barcode);
	g_print ("Title: %s\n", title);
	g_print ("Thumbnail: %s\n", img_url);
	g_print ("\n");

	g_free (barcode);
	g_free (title);
	g_free (img_url);

bail:
	num_queries--;
	if (num_queries == 0)
		g_main_loop_quit (loop);
}

static void
load_from_config (void)
{
	GKeyFile *key_file;
	char *config_path;

	config_path = g_build_filename (g_get_user_config_dir (),
					"discident-glib",
					"amazon-config.ini",
					NULL);
	key_file = g_key_file_new ();
	if (!g_key_file_load_from_file (key_file, config_path, G_KEY_FILE_NONE, NULL)) {
		g_test_skip ("Could not load Amazon config file");
		g_free (config_path);
		g_key_file_unref (key_file);
		return;
	}
	g_free (config_path);

	if (!access_key)
		access_key = g_key_file_get_string (key_file, "amz", DISCIDENT_PARAM_ACCESS_KEY, NULL);
	if (!private_key)
		private_key = g_key_file_get_string (key_file, "amz", DISCIDENT_PARAM_PRIVATE_KEY, NULL);
	if (!associate_tag)
		associate_tag = g_key_file_get_string (key_file, "amz", DISCIDENT_PARAM_ASSOCIATE_TAG, NULL);

	g_key_file_unref (key_file);
}

static GHashTable *
create_params (void)
{
	GHashTable *params;

	if (!access_key || !private_key || !associate_tag)
		load_from_config ();

	if (!access_key || !private_key || !associate_tag)
		return NULL;

	params = g_hash_table_new (g_str_hash, g_str_equal);
	g_hash_table_insert (params, DISCIDENT_PARAM_ACCESS_KEY, access_key);
	g_hash_table_insert (params, DISCIDENT_PARAM_PRIVATE_KEY, private_key);
	g_hash_table_insert (params, DISCIDENT_PARAM_ASSOCIATE_TAG, associate_tag);

	return params;
}

static void
handle_ean (const char *ean)
{
	DiscidentEan *object;
	char *title, *img_url;
	GError *error = NULL;
	gboolean ret;
	GHashTable *params;

	params = create_params ();
	object = discident_ean_new_full (ean_service, params);
	if (params)
		g_hash_table_destroy (params);

	if (!option_async) {
		ret = discident_ean_lookup_sync (object, ean, &title, &img_url, &error);
		if (ret == FALSE) {
			g_message ("discident_ean_lookup_sync() failed with: %s", error->message);
			g_error_free (error);
			g_object_unref (object);
			return;
		}
		g_print ("EAN: %s\n", ean);
		g_print ("Title: %s\n", title);
		g_print ("Thumbnail: %s\n", img_url);
		g_print ("\n");

		g_free (title);
		g_free (img_url);

		g_object_unref (object);
	} else {
		num_queries++;
		discident_ean_lookup (object, ean, NULL, discident_ean_print, NULL);
	}
}

static void
handle_gtin_uri (const char *uri)
{
	GFile *file;
	char *gtin;
	GError *error = NULL;

	file = g_file_new_for_path (uri);
	if (file == NULL) {
		g_message ("Could not convert %s", uri);
		return;
	}
	g_object_set_data (G_OBJECT (file), "wants-file-list", GINT_TO_POINTER (option_file_list));
	if (!option_async) {
		char *title;
		gtin = discident_get_gtin_file_sync (file, &error);
		g_object_unref (file);
		if (gtin == NULL) {
			g_message ("Could not get disc ID: %s", error->message);
			g_error_free (error);
			return;
		}
		g_print ("URI: %s\n", uri);

		g_print ("Disc GTIN: %s\n", gtin);
		g_print ("Query URI: "QUERY_GTIN"\n", gtin);

		title = discident_get_title_for_gtin (gtin, NULL);
		g_free (gtin);
		g_print ("Title: %s\n", title);
		g_free (title);
	} else {
		num_queries++;
		discident_get_gtin_file (file, NULL, discident_gtin_print, NULL);
		num_queries++;
		discident_get_title_file (file, NULL, discident_title_print, NULL);
		g_object_unref (file);
	}
}

int main (int argc, char **argv)
{
	GError *error = NULL;
	GOptionContext *context;
	const GOptionEntry entries[] = {
		{ "async", 'a', 0, G_OPTION_ARG_NONE, &option_async, "Use the async API", NULL },
		{ "file-list", 'f', 0, G_OPTION_ARG_NONE, &option_file_list, "Show the file list instead of the GTIN", NULL },
		{ "ean", 'e', 0, G_OPTION_ARG_NONE, &option_lookup_ean, "Look up an EAN rather than a disc", NULL },
		{ "ean-service", 's', 0, G_OPTION_ARG_STRING, &ean_service, "EAN lookup service to use", NULL },
		{ "access-key", 0, 0, G_OPTION_ARG_STRING, &access_key, "Access key", NULL },
		{ "private-key", 0, 0, G_OPTION_ARG_STRING, &private_key, "Private key", NULL },
		{ "associate-tag", 0, 0, G_OPTION_ARG_STRING, &associate_tag, "Associate Tag", NULL },
		{ G_OPTION_REMAINING, 0, 0, G_OPTION_ARG_STRING_ARRAY, &uris, NULL, "[URI...]" },
		{ NULL }
	};
	guint i;

	setlocale (LC_ALL, "");
	g_test_init (&argc, &argv, NULL);
	g_test_bug_base ("http://bugzilla.gnome.org/show_bug.cgi?id=");

	/* Parse our own command-line options */
	context = g_option_context_new ("- test parser functions");
	g_option_context_add_main_entries (context, entries, GETTEXT_PACKAGE);

	if (g_option_context_parse (context, &argc, &argv, &error) == FALSE) {
		g_print ("Option parsing failed: %s\n", error->message);
		return 1;
	}

	if (uris == NULL) {
		g_test_add_func ("/discident/ean-amz-example", test_amz1);
		g_test_add_func ("/discident/ean-amz-itemsearch", test_amz2);
		g_test_add_func ("/discident/ean-amz-cartcreate", test_amz3);
		g_test_add_func ("/discident/ean-amz-browsenodelookup", test_amz4);
		g_test_add_func ("/discident/ean-amz-similaritylookup", test_amz5);
		g_test_add_func ("/discident/rl-parse", test_rl_parse);
		g_test_add_func ("/discident/amz-parse", test_amz_parse);
		g_test_add_func ("/discident/ean", test_ean);
		g_test_add_func ("/discident/ean_fail", test_ean_fail);
		g_test_add_func ("/discident/ean-amz", test_ean_amz);
		g_test_add_func ("/discident/hash", test_hash);
		g_test_add_func ("/discident/file_list", test_file_list);
		g_test_add_func ("/discident/json", test_json);

		return g_test_run ();
	}

	for (i = 0; uris[i] != NULL; i++) {
		if (option_lookup_ean == FALSE)
			handle_gtin_uri (uris[i]);
		else
			handle_ean (uris[i]);
	}

	if (option_async) {
		loop = g_main_loop_new (NULL, FALSE);
		g_main_loop_run (loop);
	}

	return 0;
}

