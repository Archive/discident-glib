/*
   Copyright (C) 2010 Bastien Nocera

   The Gnome Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   The Gnome Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with the Gnome Library; see the file COPYING.LIB.  If not,
   write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301  USA.

   Authors: Bastien Nocera <hadess@hadess.net>

 */

#include <string.h>

#include <libsoup/soup.h>
#include <glib/gprintf.h>
#include <libxml/tree.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>

#include "discident-ean-glib.h"
#include "discident-ean-amz-glib.h"
#include "discident-error.h"

/* Full documentation at:
 * http://docs.aws.amazon.com/AWSECommerceService/latest/DG/ProgrammingGuide.html */

#define RESPONSE_NAMESPACE "http://webservices.amazon.com/AWSECommerceService/2011-08-01"

#define SEARCH_URL "https://webservices.amazon.%s/onca/xml?"

#define COMMON_PARAMETERS					\
"AWSAccessKeyId=%s&"						\
"AssociateTag=%s&"						\
"IdType=EAN&"							\
"ItemId=%s&"							\
"Operation=ItemLookup&"						\
"ResponseGroup=ItemAttributes%%2CImages&"			\
"SearchIndex=All&"						\
"Service=AWSECommerceService"

static char *
get_timestamp (void)
{
	GTimeVal tv;
	char *timestamp;
	char *ts_escaped;

	/* http://docs.aws.amazon.com/AWSECommerceService/latest/DG/rest-signature.html
	 * step 1 */

	g_get_current_time (&tv);
	/* Amazon doesn't want sub-second information */
	tv.tv_usec = 0;
	timestamp = g_time_val_to_iso8601 (&tv);
	ts_escaped = soup_uri_encode (timestamp, NULL);
	g_free (timestamp);

	return ts_escaped;
}

static int
byte_sort (gconstpointer a,
	   gconstpointer b)
{
	return memcmp (a, b, MIN (strlen (a), strlen (b)));
}

/* orig will be freed */
static char *
replace (char       *orig,
	 const char *string,
	 const char *replacement,
	 gint  start)
{
	GRegex *regex;
	char *escaped;

	regex = g_regex_new (string, 0, 0, NULL);
	escaped = g_regex_replace (regex, orig, -1, start, replacement, 0, NULL);
	g_free (orig);
	g_regex_unref (regex);

	return escaped;
}

static char *
flatten_list (GList *list)
{
	GString *string;
	GList *l;

	string = g_string_new (NULL);
	for (l = list; l != NULL; l = l->next) {
		if (l->prev != NULL)
			g_string_append_c (string, '&');
		g_string_append (string, l->data);
	}
	return g_string_free (string, FALSE);
}

#define VERIFY(x, num) if (steps != NULL && steps[num -1] != NULL) g_assert_cmpstr (x, ==, steps[num - 1])

char *
_sign_query (const char  *url,
	     const char  *timestamp,
	     const char  *private_key,
	     const char **steps)
{
	char *with_time, **params, *sorted, *final, *to_sign, *encoded_sig, *sig;
	SoupURI *uri;
	guint i;
	GList *list;
	GHmac *hmac;
	guint8 buffer[256];
	gsize len;

	with_time = g_strdup_printf ("%s&Timestamp=%s", url, timestamp);
	VERIFY (with_time, 1);

	/* http://docs.aws.amazon.com/AWSECommerceService/latest/DG/rest-signature.html
	 * step 2 */
	with_time = replace (with_time, ",", "%2C", 0);
	with_time = replace (with_time, ":", "%3A", strstr (with_time, "?") - with_time);
	VERIFY(with_time, 2);

	/* http://docs.aws.amazon.com/AWSECommerceService/latest/DG/rest-signature.html
	 * step 3 */
	uri = soup_uri_new (with_time);
	params = g_strsplit (soup_uri_get_query (uri), "&", -1);
	list = NULL;
	for (i = 0; params[i] != NULL; i++)
		list = g_list_prepend (list, params[i]);
	g_free (params);

	/* http://docs.aws.amazon.com/AWSECommerceService/latest/DG/rest-signature.html
	 * step 4 */
	list = g_list_sort (list, byte_sort);

	/* http://docs.aws.amazon.com/AWSECommerceService/latest/DG/rest-signature.html
	 * step 5 */
	sorted = flatten_list (list);

	VERIFY (sorted, 5);

	/* http://docs.aws.amazon.com/AWSECommerceService/latest/DG/rest-signature.html
	 * step 6, 7 */
	to_sign = g_strdup_printf ("GET\n%s\n/onca/xml\n%s", soup_uri_get_host (uri), sorted);
	g_free (sorted);

	VERIFY (to_sign, 7);

	/* http://docs.aws.amazon.com/AWSECommerceService/latest/DG/rest-signature.html
	 * step 8 */
	hmac = g_hmac_new (G_CHECKSUM_SHA256, (guchar *) private_key, strlen (private_key));
	g_hmac_update (hmac, (const guchar *) to_sign, -1);
	g_free (to_sign);
	len = sizeof(buffer);
	g_hmac_get_digest (hmac, buffer, &len);
	g_hmac_unref (hmac);

	sig = g_base64_encode (buffer, len);
	VERIFY (sig, 8);

	/* http://docs.aws.amazon.com/AWSECommerceService/latest/DG/rest-signature.html
	 * step 9 */

	encoded_sig = soup_uri_encode (sig, NULL);
	g_free (sig);
	encoded_sig = replace (encoded_sig, "\\+", "%2B", 0);
	VERIFY (encoded_sig, 9);

	/* http://docs.aws.amazon.com/AWSECommerceService/latest/DG/rest-signature.html
	 * step 10 */

	sig = g_strdup_printf ("Signature=%s", encoded_sig);
	list = g_list_prepend (list, sig);
	list = g_list_sort (list, byte_sort);
	sorted = flatten_list (list);
	g_list_free_full (list, g_free);

	final = g_strdup_printf ("%s://%s%s?%s",
				 soup_uri_get_scheme (uri),
				 soup_uri_get_host (uri),
				 soup_uri_get_path (uri),
				 sorted);
	g_free (sorted);
	soup_uri_free (uri);

	VERIFY (final, 10);

	return final;
}

static char *
get_search_uri (DiscidentEan *ean,
		const char   *barcode)
{
	char *uri, *timestamp, *ret;

	uri = g_strdup_printf (SEARCH_URL COMMON_PARAMETERS,
			       ean->priv->service + strlen ("amz-"),
			       ean->priv->access_key,
			       ean->priv->associate_tag, barcode);
	timestamp = get_timestamp ();

	ret = _sign_query (uri, timestamp, ean->priv->private_key, NULL);
	g_free (uri);
	g_free (timestamp);

	return ret;
}

static char *
get_value_for_xpath (xmlDocPtr           doc,
		     xmlXPathContextPtr  xpath_ctx,
		     const char         *path)
{
	xmlXPathObjectPtr xpath_obj;
	xmlNodePtr cur;
	char *ret;

	xpath_obj = xmlXPathEvalExpression (BAD_CAST (path), xpath_ctx);
	if (xpath_obj == NULL)
		return NULL;
	if (xpath_obj->nodesetval == NULL ||
	    xpath_obj->nodesetval->nodeTab == NULL) {
		xmlXPathFreeObject (xpath_obj);
		return NULL;
	}
	cur = xpath_obj->nodesetval->nodeTab[0];
	ret = g_strdup ((const char *) xmlNodeListGetString (doc, cur->xmlChildrenNode, 1));
	xmlXPathFreeObject (xpath_obj);

	return ret;
}

gboolean
_amz_parse_lookup_response (const char  *response,
			    char       **ret_title,
			    char       **ret_img_url,
			    GError     **error)
{
	xmlDocPtr doc;
	xmlXPathContextPtr xpath_ctx;
	char *error_msg;
	gboolean ret = FALSE;

	if (response == NULL) {
		g_set_error_literal (error, DISCIDENT_ERROR, DISCIDENT_ERROR_PARSE,
				     "Empty response");
		return ret;
	}

	doc = xmlParseMemory (response, strlen (response));
	if (doc == NULL)
		doc = xmlRecoverMemory (response, strlen (response));

	if(!doc ||
	   !doc->children ||
	   !doc->children->name ||
	   g_ascii_strcasecmp ((char *)doc->children->name, "ItemLookupResponse") != 0) {
		if (doc != NULL)
			xmlFreeDoc (doc);
		g_set_error_literal (error, DISCIDENT_ERROR, DISCIDENT_ERROR_PARSE,
				     "Could not parse XML in response");
		return ret;
	}

	xpath_ctx = xmlXPathNewContext(doc);

	/* Register namespace */
	xmlXPathRegisterNs (xpath_ctx, BAD_CAST ("ns"), BAD_CAST (RESPONSE_NAMESPACE));

	error_msg = get_value_for_xpath (doc, xpath_ctx, "//ns:Message");
	if (error_msg != NULL) {
		g_set_error_literal (error, DISCIDENT_ERROR, DISCIDENT_ERROR_PARSE, error_msg);
		g_free (error_msg);
		goto bail;
	}

	*ret_title = get_value_for_xpath (doc, xpath_ctx, "//ns:Title");
	if (*ret_title == NULL) {
		g_set_error_literal (error, DISCIDENT_ERROR, DISCIDENT_ERROR_PARSE,
				     "No title found in response");
		goto bail;
	}

	*ret_img_url = get_value_for_xpath (doc, xpath_ctx, "//ns:Item/ns:LargeImage/ns:URL");

	ret = TRUE;

bail:
	xmlXPathFreeContext(xpath_ctx);
	xmlFreeDoc (doc);

	return ret;
}

static SoupMessage *
create_query_message (DiscidentEan *ean,
		      const char   *barcode)
{
	SoupMessage *msg;
	char *uri;

	uri = get_search_uri (ean, barcode);
	msg = soup_message_new ("GET", uri);
	g_free (uri);

	return msg;
}

gboolean
discident_ean_amz_lookup_sync (DiscidentEan *ean,
			      const char   *barcode,
			      char        **title,
			      char        **img_url,
			      GError      **error)
{
	SoupSession *session;
	SoupMessage *msg;
	char *response;
	int ret;

	g_return_val_if_fail (DISCIDENT_IS_EAN (ean), FALSE);
	g_return_val_if_fail (title != NULL, FALSE);

	session = soup_session_new ();

	msg = create_query_message (ean, barcode);

	response = NULL;

	ret = soup_session_send_message (session, msg);
	if (SOUP_STATUS_IS_SUCCESSFUL (ret) &&
	    msg->response_body != NULL) {
		response = g_strdup (msg->response_body->data);
	}

	g_object_unref (msg);
	g_object_unref (session);

	if (response == NULL) {
		g_set_error (error, SOUP_HTTP_ERROR, ret, "Could not query EAN service");
		return FALSE;
	}

	if (_amz_parse_lookup_response (response, title, img_url, error) == FALSE) {
		g_free (response);
		if (*error == NULL)
			g_set_error (error, DISCIDENT_ERROR, DISCIDENT_ERROR_PARSE, "Failed to parse response from EAN service");
		return FALSE;
	}

	if (g_strcmp0 (*title, barcode) == 0) {
		g_free (response);
		g_set_error (error, DISCIDENT_ERROR, DISCIDENT_ERROR_EMPTY_RESPONSE, "No response for requested barcode");
		return FALSE;
	}

	g_free (response);

	return TRUE;
}

static void
got_body_query (SoupMessage *msg,
		QueryData   *data)
{
	const char *barcode;
	char *title = NULL;
	char *img_url = NULL;
	GError *error = NULL;

	if (!SOUP_STATUS_IS_SUCCESSFUL (msg->status_code) ||
	    msg->response_body == NULL) {
		g_simple_async_result_set_error (data->simple,
						 SOUP_HTTP_ERROR,
						 msg->status_code,
						 "Could not query EAN service: %s",
						 soup_status_get_phrase (msg->status_code));
		goto out;
	}

	if (_amz_parse_lookup_response (msg->response_body->data, &title, &img_url, &error) == FALSE) {
		if (error == NULL) {
			g_simple_async_result_set_error (data->simple,
							 DISCIDENT_ERROR,
							 DISCIDENT_ERROR_PARSE,
							 "Failed to parse response from EAN service");
		} else {
			g_simple_async_result_set_from_error (data->simple, error);
			g_error_free (error);
		}
		goto out;
	}

	barcode = g_object_get_data (G_OBJECT (data->simple), "barcode");
	if (g_strcmp0 (title, barcode) == 0) {
		g_simple_async_result_set_error (data->simple,
						 DISCIDENT_ERROR,
						 DISCIDENT_ERROR_EMPTY_RESPONSE,
						 "No response for requested barcode");
		g_free (title);
		g_free (img_url);
		goto out;
	}

	g_object_set_data_full (G_OBJECT (data->simple), "image-url", img_url, g_free);
	g_simple_async_result_set_op_res_gpointer (data->simple, title, NULL);

out:
	g_simple_async_result_complete_in_idle (data->simple);
	free_query_data (data);
}

void
discident_ean_amz_lookup (DiscidentEan        *ean,
			 SoupSession         *session,
			 QueryData           *data)
{
	SoupMessage *msg;

	msg = create_query_message (ean, data->barcode);
	g_signal_connect (G_OBJECT (msg), "got-body",
			  G_CALLBACK (got_body_query), data);
	soup_session_queue_message (session, msg, NULL, NULL);
}
