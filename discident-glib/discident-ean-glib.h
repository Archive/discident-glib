/*
   Copyright (C) 2010 Bastien Nocera

   The Gnome Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   The Gnome Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with the Gnome Library; see the file COPYING.LIB.  If not,
   write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301  USA.

   Authors: Bastien Nocera <hadess@hadess.net>

 */

#ifndef DISCIDENT_EAN_GLIB_H
#define DISCIDENT_EAN_GLIB_H

#include <glib.h>
#include <discident-error.h>

G_BEGIN_DECLS

#define DISCIDENT_TYPE_EAN         (discident_ean_get_type ())
#define DISCIDENT_EAN(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), DISCIDENT_TYPE_EAN, DiscidentEan))
#define DISCIDENT_EAN_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST ((k), DISCIDENT_TYPE_EAN, DiscidentEanClass))
#define DISCIDENT_IS_EAN(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), DISCIDENT_TYPE_EAN))
#define DISCIDENT_IS_EAN_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), DISCIDENT_TYPE_EAN))
#define DISCIDENT_EAN_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), DISCIDENT_TYPE_EAN, DiscidentEanClass))

typedef struct DiscidentEanPrivate DiscidentEanPrivate;

/**
 * DISCIDENT_PARAM_ACCESS_KEY:
 *
 * Access key used for Amazon lookups.
 **/
#define DISCIDENT_PARAM_ACCESS_KEY "access-key"

/**
 * DISCIDENT_PARAM_PRIVATE_KEY:
 *
 * Private key used for signing Amazon lookups.
 **/
#define DISCIDENT_PARAM_PRIVATE_KEY "private-key"

/**
 * DISCIDENT_PARAM_ASSOCIATE_TAG:
 *
 * Associate tag used for Amazon lookups.
 **/
#define DISCIDENT_PARAM_ASSOCIATE_TAG "associate-tag"

/**
 * DiscidentEan:
 *
 * All the fields in the #DiscidentEan structure are private and should never be accessed directly.
 **/
typedef struct {
	/* <private> */
	GObject              parent;
	DiscidentEanPrivate *priv;
} DiscidentEan;

/**
 * DiscidentEanClass:
 *
 * All the fields in the #DiscidentEanClass structure are private and should never be accessed directly.
 **/
typedef struct {
	/* <private> */
	GObjectClass parent_class;
} DiscidentEanClass;

GType         discident_ean_get_type     (void);
DiscidentEan *discident_ean_new          (const char *service);
DiscidentEan *discident_ean_new_full     (const char *service,
					  GHashTable *parameters);
gboolean      discident_ean_lookup_sync  (DiscidentEan *ean,
					  const char   *barcode,
					  char        **title,
					  char        **img_url,
					  GError      **error);
void discident_ean_lookup (DiscidentEan        *ean,
			   const char          *barcode,
			   GCancellable        *cancellable,
			   GAsyncReadyCallback  callback,
			   gpointer             user_data);
char * discident_ean_lookup_get_barcode (DiscidentEan  *ean,
					 GAsyncResult  *res);
char * discident_ean_lookup_finish (DiscidentEan  *ean,
				    GAsyncResult  *res,
				    char         **img_url,
				    GError       **error);

G_END_DECLS

#endif /* DISCIDENT_EAN_GLIB_H */
