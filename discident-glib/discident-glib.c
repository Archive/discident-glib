/*
   Copyright (C) 2010 Bastien Nocera

   The Gnome Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   The Gnome Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with the Gnome Library; see the file COPYING.LIB.  If not,
   write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301  USA.

   Authors: Bastien Nocera <hadess@hadess.net>

 */

#include <gio/gio.h>
#include <json-glib/json-glib.h>
#include <discident-glib.h>
#include <discident-glib-private.h>

/**
 * SECTION:discident-glib
 * @short_description: DVD lookup functions
 * @include: discident-glib/discident-glib.h
 *
 * Contains functions to lookup DVD titles from discs or local backups.
 **/

typedef struct {
	char *filename;
	guint64 size;
} ts_file;

static int
sort_ts_files (ts_file *a, ts_file *b)
{
	return g_strcmp0 (a->filename, b->filename);
}

static void
ts_file_free (ts_file *item)
{
	g_free (item->filename);
	g_free (item);
}

static void
ts_files_free (GList *items)
{
	g_list_foreach (items, (GFunc) ts_file_free, NULL);
	g_list_free (items);
}

#define FINGERPRINT_LEN 16

char *
generate_hash (const char *str, gssize len)
{
	GChecksum *md5;
	char *retval;
	guint8 buffer[FINGERPRINT_LEN];
	gsize buf_len = FINGERPRINT_LEN;

	md5 = g_checksum_new (G_CHECKSUM_MD5);
	g_checksum_update (md5, (guchar *) str, len);
	g_checksum_get_digest (md5, buffer, &buf_len);
	g_assert (buf_len == FINGERPRINT_LEN);

	retval = g_strdup_printf (
	    "%02X%02X%02X%02X-"\
	    "%02X%02X-"\
	    "%02X%02X-"\
	    "%02X%02X-"\
	    "%02X%02X%02X%02X%02X%02X",
	    buffer[0x00], buffer[0x01], buffer[0x02], buffer[0x03],
	    buffer[0x04], buffer[0x05],
	    buffer[0x06], buffer[0x07],
	    buffer[0x08], buffer[0x09],
	    buffer[0x0A], buffer[0x0B], buffer[0x0C], buffer[0x0D], buffer[0x0E], buffer[0x0F]
	    );

	g_checksum_free (md5);

	return retval;
}

static char *
file_list_to_string (GList *list)
{
	GString *data;
	GList *l;

	data = g_string_new (NULL);
	list = g_list_sort (list, (GCompareFunc) sort_ts_files);
	for (l = list; l != NULL; l = l->next) {
		ts_file *item = l->data;
		g_string_append_printf (data, ":/VIDEO_TS/%s:%"G_GUINT64_FORMAT, item->filename, item->size);
	}
	ts_files_free (list);
	return g_string_free (data, FALSE);
}

static gboolean
is_dvd (GFile *directory, GError **error)
{
	char **types;
	gboolean type_found;
	guint i;

	/* Check if we have a video DVD first */
	type_found = FALSE;
	types = g_content_type_guess_for_tree (directory);
	for (i = 0; types != NULL && types[i] != NULL; i++) {
		if (g_str_equal (types[i], "x-content/video-dvd")) {
			type_found = TRUE;
			break;
		}
	}
	if (types != NULL)
		g_strfreev (types);
	if (type_found == FALSE) {
		char *path;
		path = g_file_get_path (directory);
		g_set_error (error,
			     G_IO_ERROR,
			     G_IO_ERROR_INVALID_DATA,
			     "'%s' does not contain a DVD image",
			     path);
		g_free (path);
		return FALSE;
	}

	return TRUE;
}

static GList *
discident_add_info_to_list (GList *list,
			    GFileInfo *info)
{
	ts_file *item;

	item = g_new0 (ts_file, 1);
	item->filename = g_ascii_strup (g_file_info_get_attribute_byte_string (info, G_FILE_ATTRIBUTE_STANDARD_NAME), -1);
	item->size = g_file_info_get_attribute_uint64 (info, G_FILE_ATTRIBUTE_STANDARD_SIZE);
	g_object_unref (info);

	return g_list_prepend (list, item);
}

/**
 * discident_get_gtin_file_sync:
 * @directory: a #GFile representing a directory
 * @error: a #GError.
 *
 * Return the GTIN of the DVD location at @directory. Note that this
 * function does blocking I/O. See discident_get_gtin_file()
 * for a function that does not.
 *
 * Returns: a string containing the GTIN for the DVD or %NULL in case of errors.
 * Free the returned string with g_free() when done.
 **/
char *
discident_get_gtin_file_sync (GFile *directory, GError **error)
{
	GFileEnumerator *e;
	GFile *videodir;
	GFileInfo *info;
	char *gtin, *file_list_str;
	GList *items;
	gboolean wants_file_list;
	GError *err = NULL;

	if (is_dvd (directory, error) == FALSE)
		return NULL;

	videodir = g_file_get_child (directory, "VIDEO_TS");
	if (g_file_query_exists (videodir, NULL) == FALSE) {
		g_object_unref (videodir);
		videodir = g_file_get_child (directory, "video_ts");
	}

	e = g_file_enumerate_children (videodir,
				       G_FILE_ATTRIBUTE_STANDARD_NAME","G_FILE_ATTRIBUTE_STANDARD_SIZE,
				       G_FILE_QUERY_INFO_NONE,
				       NULL,
				       error);
	g_object_unref (videodir);
	if (e == NULL)
		return NULL;

	/* Gather all the filenames and their sizes */
	items = NULL;
	while ((info = g_file_enumerator_next_file (e, NULL, &err)) != NULL) {
		items = discident_add_info_to_list (items, info);
	}

	if (err != NULL) {
		g_propagate_error (error, err);
		ts_files_free (items);
		g_file_enumerator_close (e, NULL, NULL);
		return NULL;
	}
	g_file_enumerator_close (e, NULL, NULL);

	/* Build up the string that we hash to produce the buffer. */
	file_list_str = file_list_to_string (items);

	/* HACK for debugging and testing */
	wants_file_list = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (directory), "wants-file-list"));
	if (wants_file_list)
		return file_list_str;

	/* Calculate the MD5SUM */
	gtin = generate_hash (file_list_str, -1);
	g_free (file_list_str);

	return gtin;
}

#define FILE_LIST_NUM_ITEMS 10

static void
on_videodir_enumerate_next_files (GObject      *source_object,
				  GAsyncResult *res,
				  gpointer      user_data)
{
	GSimpleAsyncResult *simple = G_SIMPLE_ASYNC_RESULT (user_data);
	GFileEnumerator *enumerator;
	GCancellable *cancellable;
	GList *file_infos, *list, *l;
	GError *error = NULL;

	enumerator = G_FILE_ENUMERATOR (source_object);
	file_infos = g_file_enumerator_next_files_finish (enumerator, res, &error);
	list = g_object_get_data (G_OBJECT (simple), "file-list");

	if (error) {
		if (!g_error_matches (error, G_IO_ERROR, G_IO_ERROR_CANCELLED))
			g_warning ("%s", error->message);

		ts_files_free (list);

		g_error_free (error);
		return;
	}

	if (!file_infos) {
		char *file_list_str, *gtin;
		gboolean wants_file_list;

		g_file_enumerator_close_async (enumerator,
					       G_PRIORITY_DEFAULT,
					       NULL, NULL, NULL);

		file_list_str = file_list_to_string (list);
		wants_file_list = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (simple), "wants-file-list"));
		if (wants_file_list) {
			g_simple_async_result_set_op_res_gpointer (simple, file_list_str, NULL);
		} else {
			gtin = generate_hash (file_list_str, -1);
			g_simple_async_result_set_op_res_gpointer (simple, gtin, NULL);
			g_free (file_list_str);
		}
		g_simple_async_result_complete_in_idle (simple);
		g_object_unref (simple);

		return;
	}

	for (l = file_infos; l != NULL; l = l->next) {
		GFileInfo *info = l->data;
		list = discident_add_info_to_list (list, info);
	}
	g_list_free (file_infos);

	g_object_set_data (G_OBJECT (simple), "file-list", list);

	/* And look for more files */
	cancellable = g_object_get_data (G_OBJECT (simple), "cancellable");
	g_file_enumerator_next_files_async (enumerator,
					    FILE_LIST_NUM_ITEMS,
					    G_PRIORITY_DEFAULT,
					    cancellable,
					    on_videodir_enumerate_next_files,
					    simple);
}

static void
on_videodir_enumerate_loaded (GObject      *source_object,
			      GAsyncResult *res,
			      gpointer      user_data)
{
	GSimpleAsyncResult *simple = G_SIMPLE_ASYNC_RESULT (user_data);
	GFile *video_ts_dir;
	GError *error = NULL;
	GFileEnumerator *enumerator;
	GCancellable *cancellable;
	gboolean lower_case;

	cancellable = g_object_get_data (G_OBJECT (simple), "cancellable");
	lower_case = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (simple), "lower-case"));
	video_ts_dir = G_FILE (source_object);
	enumerator = g_file_enumerate_children_finish (video_ts_dir,
						       res,
						       &error);
	if (enumerator == NULL) {
		if (lower_case == FALSE) {
			GFile *parent;
			/* Try in lower-case now */
			parent = g_file_get_parent (video_ts_dir);
			/* The video_ts_dir only has one floating reference */
			video_ts_dir = g_file_get_child (parent, "video_ts");
			g_object_set_data (G_OBJECT (simple), "lower-case", GINT_TO_POINTER (TRUE));
			g_object_unref (parent);

			g_file_enumerate_children_async (video_ts_dir,
							 G_FILE_ATTRIBUTE_STANDARD_NAME","G_FILE_ATTRIBUTE_STANDARD_SIZE,
							 G_FILE_QUERY_INFO_NONE,
							 G_PRIORITY_DEFAULT,
							 cancellable,
							 on_videodir_enumerate_loaded,
							 simple);
			g_object_unref (video_ts_dir);
			return;
		}

		/* Error? */
		g_simple_async_result_set_from_error (simple, error);
		g_simple_async_result_complete_in_idle (simple);
		g_object_unref (simple);
		g_error_free (error);
		return;
	}

	g_file_enumerator_next_files_async (enumerator,
					    FILE_LIST_NUM_ITEMS,
					    G_PRIORITY_DEFAULT,
					    cancellable,
					    on_videodir_enumerate_next_files,
					    simple);
}

/**
 * discident_get_gtin_file:
 * @directory: a #GFile representing a directory
 * @cancellable: optional #GCancellable object, %NULL to ignore.
 * @callback: a #GAsyncReadyCallback to call when the request is satisfied
 * @user_data: the data to pass to callback function
 *
 * Asynchronously gets the GTIN of the DVD tree location in
 * @directory. This function does not use the Internet.
 *
 * When the operation is finished, @callback will be called. You can then call
 * discident_get_gtin_file_finish() to get the result of the operation.
 **/
void
discident_get_gtin_file  (GFile               *directory,
			  GCancellable        *cancellable,
			  GAsyncReadyCallback  callback,
			  gpointer             user_data)
{
	GSimpleAsyncResult *simple;
	GFile *video_ts_dir;
	gboolean wants_file_list;

	simple = g_simple_async_result_new (G_OBJECT (directory),
					    callback,
					    user_data,
					    discident_get_gtin_file);

	if (cancellable != NULL) {
		g_object_set_data_full (G_OBJECT (simple), "cancellable",
					g_object_ref (cancellable), g_object_unref);
	}
	/* HACK for debugging and testing */
	wants_file_list = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (directory), "wants-file-list"));
	if (wants_file_list) {
		g_object_set_data (G_OBJECT (simple), "wants-file-list",
				   GINT_TO_POINTER (wants_file_list));
	}

	video_ts_dir = g_file_get_child (directory, "VIDEO_TS");
	g_file_enumerate_children_async (video_ts_dir,
					 G_FILE_ATTRIBUTE_STANDARD_NAME","G_FILE_ATTRIBUTE_STANDARD_SIZE,
					 G_FILE_QUERY_INFO_NONE,
					 G_PRIORITY_DEFAULT,
					 cancellable,
					 on_videodir_enumerate_loaded,
					 simple);
	g_object_unref (video_ts_dir);
}

/**
 * discident_get_gtin_file_finish:
 * @directory: a #GFile representing a directory
 * @res: a #GAsyncResult.
 * @error: a #GError.
 *
 * Finishes getting the GTIN for the DVD tree at @directory. See 
 * discident_get_gtin_file().
 *
 * Returns: the GTIN for the DVD or %NULL in case of errors.
 * Free the returned string with g_free() when done.
 **/
char *
discident_get_gtin_file_finish (GFile               *directory,
			     GAsyncResult        *res,
			     GError             **error)
{
	GSimpleAsyncResult *simple = G_SIMPLE_ASYNC_RESULT (res);
	char *ret;

	g_warn_if_fail (g_simple_async_result_get_source_tag (simple) == discident_get_gtin_file);

	ret = NULL;

	if (g_simple_async_result_propagate_error (simple, error))
		goto out;

	ret = g_simple_async_result_get_op_res_gpointer (simple);

out:
	return ret;
}

static GFile *
get_file_for_gtin (const char *gtin)
{
	GFile *file;
	char *str;

	str = g_strdup_printf (QUERY_GTIN, gtin);
	file = g_file_new_for_uri (str);
	g_free (str);

	return file;
}

static char *
get_json_for_gtin (const char *gtin, GError **error)
{
	GFile *file;
	char *contents;

	file = get_file_for_gtin (gtin);

	if (g_file_load_contents (file, NULL, &contents, NULL, NULL, NULL) == FALSE) {
		g_object_unref (file);
		return NULL;
	}
	g_object_unref (file);

	return contents;
}

static char *
discident_get_title_for_json_content (const char *contents,
				      GError    **error)
{
	JsonParser *parser;
	JsonNode *root;
	JsonObject *object;
	char *title;

	if (g_str_equal (contents, "404") != FALSE)
		return NULL;

	parser = json_parser_new ();
	if (json_parser_load_from_data (parser, contents, -1, error) == FALSE) {
		g_object_unref (parser);
		return NULL;
	}

	root = json_parser_get_root (parser);
	object = json_node_get_object (root);

	title = g_strdup (json_node_get_string (json_object_get_member (object, "title")));

	g_object_unref (parser);

	return title;
}

char *
discident_get_title_for_gtin (const char *gtin,
			      GError    **error)
{
	char *contents, *title;

	contents = get_json_for_gtin (gtin, error);
	if (contents == NULL)
		return NULL;

	title = discident_get_title_for_json_content (contents, error);

	g_free (contents);

	return title;
}

/**
 * discident_get_title_sync:
 * @directory: a #GFile representing a directory
 * @error: a #GError.
 *
 * Returns the title of the DVD located at @directory. Note that this
 * function does blocking I/O. See discident_get_title_file()
 * for a function that does not.
 *
 * Returns: a string containing the title or %NULL in case of errors.
 * Free the returned string with g_free() when done.
 **/
char *
discident_get_title_sync (GFile   *directory,
			  GError **error)
{
	char *gtin;
	char *title;

	gtin = discident_get_gtin_file_sync (directory, error);
	if (gtin == NULL)
		return NULL;

	title = discident_get_title_for_gtin (gtin, error);
	g_free (gtin);

	return title;
}

static void
on_query_data_loaded (GObject      *source_object,
		      GAsyncResult *res,
		      gpointer      user_data)
{
	GSimpleAsyncResult *simple = G_SIMPLE_ASYNC_RESULT (user_data);
	GFile *query;
	GError *error = NULL;
	char *contents, *title;

	query = G_FILE (source_object);
	if (g_file_load_contents_finish (query,
					 res,
					 &contents,
					 NULL,
					 NULL,
					 &error) == FALSE) {
		g_simple_async_result_set_from_error (simple, error);
		g_simple_async_result_complete_in_idle (simple);
		g_object_unref (simple);
		g_error_free (error);
		return;
	}

	title = discident_get_title_for_json_content (contents, &error);
	g_free (contents);

	if (title == NULL && error != NULL) {
		g_simple_async_result_set_from_error (simple, error);
		g_simple_async_result_complete_in_idle (simple);
		g_object_unref (simple);
		g_error_free (error);
		return;
	}

	g_simple_async_result_set_op_res_gpointer (simple, title, NULL);
	g_simple_async_result_complete_in_idle (simple);
	g_object_unref (simple);
}

static void
on_discident_got_gtin (GObject      *source_object,
		       GAsyncResult *res,
		       gpointer      user_data)
{
	GSimpleAsyncResult *simple = G_SIMPLE_ASYNC_RESULT (user_data);
	GCancellable *cancellable;
	GFile *directory, *query;
	GError *error = NULL;
	char *gtin;

	cancellable = g_object_get_data (G_OBJECT (simple), "cancellable");
	directory = G_FILE (source_object);
	gtin = discident_get_gtin_file_finish (directory,
					       res,
					       &error);
	if (gtin == NULL) {
		g_simple_async_result_set_from_error (simple, error);
		g_simple_async_result_complete_in_idle (simple);
		g_object_unref (simple);
		g_error_free (error);
		return;
	}

	query = get_file_for_gtin (gtin);
	g_free (gtin);
	g_file_load_contents_async (query,
				    cancellable,
				    on_query_data_loaded,
				    simple);
	g_object_unref (query);
}

/**
 * discident_get_title_file:
 * @directory: a #GFile representing a directory
 * @cancellable: optional #GCancellable object, %NULL to ignore.
 * @callback: a #GAsyncReadyCallback to call when the request is satisfied
 * @user_data: the data to pass to callback function
 *
 * Asynchronously gets the title of the DVD tree located in
 * @directory using the DiscIdent web service.
 *
 * When the operation is finished, @callback will be called. You can then call
 * discident_get_title_file_finish() to get the result of the operation.
 **/
void
discident_get_title_file (GFile               *directory,
			  GCancellable        *cancellable,
			  GAsyncReadyCallback  callback,
			  gpointer             user_data)
{
	GSimpleAsyncResult *simple;

	simple = g_simple_async_result_new (G_OBJECT (directory),
					    callback,
					    user_data,
					    discident_get_title_file);

	if (cancellable != NULL) {
		g_object_set_data_full (G_OBJECT (simple), "cancellable",
					g_object_ref (cancellable), g_object_unref);
	}

	discident_get_gtin_file (directory,
				 cancellable,
				 on_discident_got_gtin,
				 simple);
}

/**
 * discident_get_title_file_finish:
 * @directory: a #GFile representing a directory
 * @res: a #GAsyncResult.
 * @error: a #GError.
 *
 * Finishes a title get operation. See discident_get_title_file().
 *
 * Returns: a string containing the title or %NULL in case of errors.
 * Free the returned string with g_free() when done.
 **/
char *
discident_get_title_file_finish (GFile               *directory,
				 GAsyncResult        *res,
				 GError             **error)
{
	GSimpleAsyncResult *simple = G_SIMPLE_ASYNC_RESULT (res);
	char *ret;

	g_warn_if_fail (g_simple_async_result_get_source_tag (simple) == discident_get_title_file);

	ret = NULL;

	if (g_simple_async_result_propagate_error (simple, error))
		goto out;

	ret = g_simple_async_result_get_op_res_gpointer (simple);

out:
	return ret;
}

